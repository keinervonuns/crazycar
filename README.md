Well running code for the FH-Joanneum CrazyCar competition (standard msp430 controller on FH-custom Board).

Runs great if the back of the car is weighted and the pid values are calibrated correctly.
All sensor values are measured starting from the sensor case.

!!Attention!!
Does not work well if the rear axle is bent (drives more like a hopping rabbit)!!

ECE17
Team name: Händel
\select@language {ngerman}
\contentsline {section}{\numberline {1}Allgemein}{1}
\contentsline {section}{\numberline {2}Crazy Car Platine\IeC {\textendash }Einf\IeC {\"u}hrung}{2}
\contentsline {subsection}{\numberline {2.1}Das Crazy Car}{2}
\contentsline {subsubsection}{\numberline {2.1.1}Der Mikrocontroller}{2}
\contentsline {subsection}{\numberline {2.2}Abbildung im Code Composer}{2}
\contentsline {subsubsection}{\numberline {2.2.1}Hardwarelayer}{3}
\contentsline {subsubsection}{\numberline {2.2.2}Driver}{3}
\contentsline {subsubsection}{\numberline {2.2.3}Application}{3}
\contentsline {section}{\numberline {3}IO-Port Konfiguration}{4}
\contentsline {subsection}{\numberline {3.1}Pineigenschaften MSP430}{4}
\contentsline {subsection}{\numberline {3.2}IO Konfiguration im Code Composer}{4}
\contentsline {subsubsection}{\numberline {3.2.1}Maskierungsarten}{4}
\contentsline {subsubsection}{\numberline {3.2.2}Einf\IeC {\"u}hrendes Beispiel einer IO-Port Konfiguration}{5}
\contentsline {subsubsection}{\numberline {3.2.3}I-O Konfiguration f\IeC {\"u}r das crazy car}{6}
\contentsline {section}{\numberline {4}Rechendauer Performance Test}{9}
\contentsline {subsection}{\numberline {4.1}Code}{9}
\contentsline {subsection}{\numberline {4.2}Messung am Oszilloskop}{9}
\contentsline {section}{\numberline {5}Allgemeine Interrupt Steuerung}{11}
\contentsline {subsection}{\numberline {5.1}Hintergrundleuchten}{11}
\contentsline {subsection}{\numberline {5.2}Kontaktprellen}{13}
\contentsline {subsection}{\numberline {5.3} Interrupt den zeitlichen Ablauf}{13}
\contentsline {subsection}{\numberline {5.4}Was ist Interrupt Nesting}{13}
\contentsline {subsection}{\numberline {5.5}Welche Funktion besitzt das GIE Bit}{13}
\contentsline {section}{\numberline {6}Unified Clock System}{14}
\contentsline {subsection}{\numberline {6.1}Konfiguration der Sub Master Clock}{14}
\contentsline {subsubsection}{\numberline {6.1.1}Messen der Submasterclock mit dem Oszilloskop}{16}
\contentsline {section}{\numberline {7}Timer}{17}
\contentsline {subsection}{\numberline {7.1}Unterschied zwischen Timer A und Timer B}{17}
\contentsline {subsection}{\numberline {7.2}Crazy Car Display mit Hilfe von Timer B blinken lassen}{17}
\contentsline {subsubsection}{\numberline {7.2.1}Exemplarisches \IeC {\"A}ndern einer Registereigenschaft}{17}
\contentsline {subsubsection}{\numberline {7.2.2}Z\IeC {\"a}hlfunktion}{18}
\contentsline {subsubsection}{\numberline {7.2.3}Umsetzung im Codecomposer}{18}
\contentsline {subsubsection}{\numberline {7.2.4}Messung der Blinkzeit mit dem Oszilloskop}{20}
\contentsline {section}{\numberline {8}Erzeugung eines PWM Signals mit TimerA}{21}
\contentsline {subsection}{\numberline {8.1}Timer Up Mode am Timer A}{21}
\contentsline {section}{\numberline {9}Ansteuerung des Lenkservos am Crazy Car}{22}
\contentsline {section}{\numberline {10}SPI}{26}
\contentsline {subsubsection}{\numberline {10.0.1}Initialisieren der UCB1}{27}
\contentsline {subsubsection}{\numberline {10.0.2}\IeC {\"U}bertragung}{28}
\contentsline {subsubsection}{\numberline {10.0.3}Senden von Main}{30}
\contentsline {subsubsection}{\numberline {10.0.4}Messung}{30}
\contentsline {section}{\numberline {11}Richtigkeitsbeglaubigung}{32}

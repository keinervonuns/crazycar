//schema1.c - 7.Okt 2018
//Schematisches Aendern der Pineigenschaften

#include <msp430.h>
//Inkludieren des msp430.h file. In ihm ist BIT3 und P1DIR definiert.

#define RPM_SENSOR BIT3
//Legt den Ausdruck RPM_SENSOR als 0000 0100 fest.

P1DIR &= (~RPM_SENSOR);
//Durch Maskieren von Port1 mit 1111 1011, wird die Direction von PIN3 0, was einem Eingang entspricht 
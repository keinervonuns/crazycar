//Code zum PErformancevergleich Integer und Gleitkomma-Berechnung

#include <msp430.h> 
#include <hal_general.h>
#include <hal_gpio.h>

void main(void)
{
    WDTCTL = WDTPW | WDTHOLD;
    HAL_Init();
    
    int int1 = 500;     //Integer Zahl 1
    int int2 = 20;      //Integer Zahl 2
    int interg = 0;     //Integer Zahl 3 - fur Ergebnis
    
    float float1 = 500.55;  //Gleitkomma Zahl 1
    float float2 = 20.22;   //Gleitkomma Zahl 2
    float floaterg = 0.0;   //Gleitkomma Zahl 3 - fur Ergebnis

    while(1) {
        
        LCD_BACKLIGHT_ON;
        interg = int1 * int2;
        //LCD_BACKLIGHT ist fur die Dauer der Integer-Berechnung HIGH
        LCD_BACKLIGHT_OFF;
        
        //LCD_BACKLIGHT ist LOW
        
        LCD_BACKLIGHT_ON;
        floaterg = float1 * float2;
        //LCD_BACKLIGHT ist fur die Dauer der Gleitkomma-Berechnung HIGH
        LCD_BACKLIGHT_OFF;
        
        //LCD_BACKLIGHT ist LOW
        
    }
}

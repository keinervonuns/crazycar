#include <msp430.h> 
#include <hal_general.h>
#include <hal_gpio.h>

extern ButtonCom button;

void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	HAL_Init();
	/*int int1 = 500;
	int int2 = 20;
	int interg = 0;
	float float1 = 500.55;
	float float2 = 20.22;
	float floaterg = 0.0;
	*/


	while(1) {
	   /* LCD_BACKLIGHT_ON;
	    interg = int1 * int2;
	    LCD_BACKLIGHT_OFF;
	    LCD_BACKLIGHT_ON;
        floaterg = float1 * float2;
        LCD_BACKLIGHT_OFF;
        */

	    if(button.active == 1) {
	        switch(button.button) {
	        case 6:             //Start
	            LCD_BACKLIGHT_ON;
	            break;
	        case 7:             //Stop
	            LCD_BACKLIGHT_OFF;
	            break;
	        }
	        button.active = 0;

	    }

	}
}
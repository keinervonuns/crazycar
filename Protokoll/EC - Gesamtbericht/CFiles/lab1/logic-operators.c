//Beispiel von Maskierung

#include<msp430.h>      //Zur Verwendung der BITX ausdrucke
#define PXDIR P1Dir     //Fur dieses Beispiel sei PXDIR P1Dir


//Zum Setzen auf 1 eines Bits, ohne den Zustand der anderen zu verandern
//======================================================================

PXDIR |= BIT0;  

/*
  UUUU UUUU     - Zustand von PXDIR
|=0000 0001     - BIT 0
-----------
  UUUU UUU0     - Neuer Zustand von PXDIR
*/


//Zum Setzen auf 0 eines Bits, ohne den Zustand der anderen zu verandern
//======================================================================

PXDIR &= ~BIT0;

/*
  UUUU UUU1     - Zustand von PXDIR
&=1111 1110     - ~BIT 0
-----------
  UUUU UUU0     - Neuer Zustand von PXDIR
*/
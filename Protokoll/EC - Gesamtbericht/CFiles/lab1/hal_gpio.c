#include <hal_gpio.h>
#include <msp430.h>

void HAL_GPIO_Init(void){

    //Defaults

    P1SEL = sel_def;
    P1DIR = dir_def;
    P1OUT = out_def;

    P2SEL = sel_def;
    P2DIR = dir_def;
    P2OUT = out_def;

    P3SEL = sel_def;
    P3DIR = dir_def;
    P3OUT = out_def;

    P4SEL = sel_def;
    P4DIR = dir_def;
    P4OUT = out_def;

    P5SEL = sel_def;
    P5DIR = dir_def;
    P5OUT = out_def;

    P6SEL = sel_def;
    P6DIR = dir_def;
    P6OUT = out_def;

    P7SEL = sel_def;
    P7DIR = dir_def;
    P7OUT = out_def;

    P8SEL = sel_def;
    P8DIR = dir_def;
    P8OUT = out_def;

    P9SEL = sel_def;
    P9DIR = dir_def;
    P9OUT = out_def;

    //Port1
    P1DIR &= (~RPM_SENSOR) & (~RPM_SENSOR_DIR) & (~START_BUTTON);

    //Port2
    P2DIR &= ~DEBUG_TXD;

    //Port3
    //all out

    //Port4
    P4DIR &= (~LINE_FOLLOW_1)&(~LINE_FOLLOW_2)&(~LINE_FOLLOW_3)&(~LINE_FOLLOW_4)&(~LINE_FOLLOW_5);

    //Port5

    //Port6
    P6DIR &= (~DISTANCE_RIGHT)&(~DISTANCE_LEFT)&(~DISTANCE_FRONT)&(~VBAT_MEASURE);

    //Port7
    //QUARZ

    //Port8
    P8DIR &= (~UART_RXD_AUX)&(~LCD_SPI_MISO);

    //Port9
}

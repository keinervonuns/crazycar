//doku

#ifndef __HAL_USCIB1__
    #define __HAL_USCIB1__

    typedef struct {
        union{
            unsigned char R;
            struct {
                unsigned char TxSuc:1; // Bit=1 wenn Daten ubertragen wurden
                unsigned char dummy:7;
            }B;
        }Status;
            struct {
                unsigned char len; // Lange der Daten in Bytes die ubertragen werden
                unsigned char cnt; // Index auf momentan ubertragene Daten
                unsigned char Data[256]; // Tx Daten Array
        }TxData;
            struct {
                unsigned char len; // Lange der empfangenen Daten
                unsigned char Data[256]; // Rx Daten Array
        }RxData;
    }USCIB1_SPICom;

    void HAL_USCIB1_Transmit(void);
    void HAL_USCIB1_Init();

#endif

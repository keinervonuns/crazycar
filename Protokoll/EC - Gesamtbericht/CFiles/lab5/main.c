//doku main.c

#include <msp430.h> 
#include <hal_general.h>
#include <hal_gpio.h>
#include <driver_general.h>
#include <hal_usciB1.h>

//...

extern USCIB1_SPICom spi_status;

void main(void)
{
	//...
	
	spi_status.TxData.len = 2;					//set length to 2 byte
	spi_status.Status.B.TxSuc = 1;				//start condition
    spi_status.TxData.Data[0] = 0b11001100;		//data 1
    spi_status.TxData.Data[1] = 0b11101110;		//data 2

	while(1) {

	    HAL_USCIB1_Transmit();
		
	}
}

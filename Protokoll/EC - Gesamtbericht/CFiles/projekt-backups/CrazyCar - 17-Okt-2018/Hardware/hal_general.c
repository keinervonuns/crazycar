#include <hal_pmm.h>
#include <hal_wdt.h>
#include <hal_gpio.h>

ButtonCom button;

void HAL_Init(){
    HAL_Wdt_Init();
    HAL_PMM_Init();
    HAL_GPIO_Init();
}

//Dieser Kommentar ist eine Teständerung für Gitlab

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
%.obj: ../%.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"D:/ti/ccsv8/tools/compiler/ti-cgt-msp430_18.1.3.LTS/bin/cl430" -vmspx --data_model=restricted -Ooff --opt_for_speed=0 --use_hw_mpy=F5 --include_path="D:/ti/ccsv8/ccs_base/msp430/include" --include_path="C:/Users/chris/Projekte/crazycar/CrazyCar" --include_path="D:/ti/ccsv8/tools/compiler/ti-cgt-msp430_18.1.3.LTS/include" --include_path="C:/Users/chris/Projekte/crazycar/CrazyCar/Application" --include_path="C:/Users/chris/Projekte/crazycar/CrazyCar/Driver" --include_path="C:/Users/chris/Projekte/crazycar/CrazyCar/Hardware" --advice:power=all --define=__MSP430F5335__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '



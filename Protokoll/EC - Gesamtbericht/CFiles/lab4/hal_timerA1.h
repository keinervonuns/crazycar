#ifndef __HAL_TIMERA1__
    #define __HAL_TIMERA1__

    #define PERIODENDAUER   41666   //Periodendauer 60Hz
    #define PWM_INIT        2500    //1,5ms -> Rader stehen gerade
    #define SERVO_MAX       4500
    #define SERVO_MIN       2500
    #define SERVO_MID       3500

    void HAL_TimerA1_Init();

#endif

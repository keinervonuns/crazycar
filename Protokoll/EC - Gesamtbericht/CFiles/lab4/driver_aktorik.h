#ifndef __DRV_AKTORIK__
    #define __DRV_AKTORIK__

    #define STEER_MAX   100		//Maximalwert Lenkung
    #define STEER_MIN   -100	//Minimalwert Lenkung

    void Driver_SetSteering(signed char position);
    void Driver_SetThrottle(char speed);
    void Driver_SetSteeringInit();
    void Driver_ESC_Init();
    void createPulses(int pulsewidth, unsigned char amount);

#endif

/*
-Kommentiert am 5-Dez-2018

-Wurde noch nicht mit Laborversion vom 5.12.2018 verglichen

*/

#include <hal_adc12.h>
#include <msp430.h>
#include <hal_gpio.h>

extern ADC12Com ADC_Data; 		//aus hal_general

void HAL_ADC12_Init(void){
	
	//ADC12_A registor for data transmission
	//----------------------------------------------------------
	
    ADC12CTL0 &= ~ADC12ENC; 	//disable conversation

	
    //REFCTL0 register
	//----------------------------------------------------------
	
	//Settings for behaviour
	ADC12CTL1 |= ADC12SSEL_3;       //Clock Source = SMCLK @ 2.5MHz
	ADC12CTL1 |= ADC12CONSEQ_3;     //Repeat Sequence of channels
	
	//Reference voltage
    REFCTL0 |= REFOUT;		//Reference output available externally
    REFCTL0 |= REFON; 		//Set because intern reference voltage is used

	//Set ADC12MEM0 - ADC12MEM15 to 1024 ADC12CLK Cycles
    ADC12CTL0 |= ADC12SHT0_15;      //for ADC12MEM0 to ADC12MEM7
    ADC12CTL0 |= ADC12SHT1_15; 		//for ADC12MEM8 to ADC12MEM15
    
	//Other settings
	ADC12CTL0 |= ADC12MSC; 			/*
		The first rising edge of the SHI signal triggers the sampling timer, 
		but further sample-and-conversions are performed automatically as 
		soon as the prior conversion is completed.
		*/
    ADC12CTL0 |= ADC12REF2_5V; 		/*
		sets the reference voltare to 2.5V, requires ADC12REFON*/
    ADC12CTL0 |= ADC12REFON;  		//required

    ADC12CTL1 |= ADC12SHS_0;        //TimerB0 starter
    ADC12CTL1 |= ADC12SHP;          /*
		source of the sampling signal sampling timer B */
    ADC12CTL2 |= ADC12RES_2;        /*
	12Bit ADC12_A resolution (13 clock cycle conversion time)*/


    //The ADC12MCTL register is for reading from inputs.
    //It is also repeated as a sequence.
    //----------------------------------------------------------
    //See crazycar schematics for what is on which port.

    //Distnace Right
    ADC12MCTL0 |= ADC12SREF_1;      //VeREF bis AVSS
    ADC12MCTL0 |= ADC12INCH_0;      //Selects Input Channel A0

    //Distance Left
    ADC12MCTL1 |= ADC12SREF_1;      //VeREF bis AVSS
    ADC12MCTL1 |= ADC12INCH_1;      //Selects Input Channel A1

    //Distance Front
    ADC12MCTL2 |= ADC12SREF_1;      //VeREF bis AVSS
    ADC12MCTL2 |= ADC12INCH_2;      //Selects Input Channel A2

    //VBattery
    ADC12MCTL3 |= ADC12SREF_1;      //VeREF bis AVSS
    ADC12MCTL3 |= ADC12INCH_3;      //Selects Input Channel A3

    ADC12MCTL3 |= ADC12EOS;         //End of Sequence enable

    ADC12IE = 0xffff;               //Enable all interrupts 
	
	
	//ADC12_A registor for data transmission
	//----------------------------------------------------------
	
    ADC12CTL0 |= ADC12ON; 			//ADC12_A registor on - turns ADC on
    ADC12CTL0 |= ADC12ENC;   		//Enable conversation
}

#pragma vector=ADC12_VECTOR
   __interrupt void ADC12_isr(void)
   {
	    //ISR for ADC
	    //ADC12MEM0 contains the input of ADC12MCTL0 and so on.
		//----------------------------------------------------------
		
		//The value of the memory register is written into the ADC buffer
		//for further output
        ADC_Data.ADCBuffer[0] = ADC12MEM0;
        ADC_Data.ADCBuffer[1] = ADC12MEM1;
        ADC_Data.ADCBuffer[2] = ADC12MEM2;
        ADC_Data.ADCBuffer[3] = ADC12MEM3;        
   
		ADC12IFG = 0x0000; 					//Resets the Interrupt register
        ADC_Data.Status.B.ADCrdy = 1; 	 	//Starts transmission to display
		
        ADC12CTL0 |= ADC12ENC; /*  			
			After conversation the conversation has to be enabled again*/

   }

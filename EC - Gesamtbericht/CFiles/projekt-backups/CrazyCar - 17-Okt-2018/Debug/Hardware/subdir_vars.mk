################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Hardware/hal_general.c \
../Hardware/hal_gpio.c \
../Hardware/hal_pmm.c \
../Hardware/hal_wdt.c 

C_DEPS += \
./Hardware/hal_general.d \
./Hardware/hal_gpio.d \
./Hardware/hal_pmm.d \
./Hardware/hal_wdt.d 

OBJS += \
./Hardware/hal_general.obj \
./Hardware/hal_gpio.obj \
./Hardware/hal_pmm.obj \
./Hardware/hal_wdt.obj 

OBJS__QUOTED += \
"Hardware\hal_general.obj" \
"Hardware\hal_gpio.obj" \
"Hardware\hal_pmm.obj" \
"Hardware\hal_wdt.obj" 

C_DEPS__QUOTED += \
"Hardware\hal_general.d" \
"Hardware\hal_gpio.d" \
"Hardware\hal_pmm.d" \
"Hardware\hal_wdt.d" 

C_SRCS__QUOTED += \
"../Hardware/hal_general.c" \
"../Hardware/hal_gpio.c" \
"../Hardware/hal_pmm.c" \
"../Hardware/hal_wdt.c" 



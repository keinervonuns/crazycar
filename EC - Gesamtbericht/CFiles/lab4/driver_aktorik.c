/*
Diese Funktion behandelt Lenkung und Beschleunigung
*/

#include <msp430.h>
#include <hal_gpio.h>
#include <driver_aktorik.h>

unsigned char counter;			//Pulscounter

void Driver_SetSteering(signed char position) {
	
	/*
	Diese Funktion bewegt den Servomotor der Lenkung, da in hal_gpio.c
	das Ergebnis von CCR2 auf Port P3.3 geroutet wurde.
	*/
	
    if ((position >= 0) && (position <= STEER_MAX)){
		
        TA1CCR2 = (position * 10) + 3500;
		//Im Compare Register steht ein Wert zwischen 3500 und 4500(MAX)
		
    } else if((position < 0) && (position >= STEER_MIN)){
		
        TA1CCR2 = (position * -10) + 2500;
		//Im Compare Register steht ein Wert zwischen 2500(MIN) und 3500
    }
}

void Driver_SetSteeringInit() {
    Driver_SetSteering(0);
}

void Driver_SetThrottle(char speed) {

}

void Driver_ESC_Init() {
	
	/*
	Kalibrieren des Motors
	Pulsbreite in Schritte (Hz*s)
	*/
	
    createPulses(2500,140);      
    createPulses(5000,140);
    createPulses(7500,140);
    createPulses(10000,140);
}

void createPulses(int pulsewidth, unsigned char amount) {
	
	//Diese Funktion generiert amount viele Impulse
	
    counter = 0;
    while(counter <= amount){
        TA1CCR1 = pulsewidth;
    }
}

#pragma vector=TIMER1_A0_VECTOR
   __interrupt void timera0_isr(void)
   {
	   /*
	   Dieser Interrupt wird immer dann ausgelost wenn der Timer auf
	   den Referenzwert stosst?
	   
	   Durch die reset/set Modus von TimerA bedeutet das einen Impuls.
	   */
	   
       counter++;			//Impuls Counter +1
       TA0CCTL0 &= ~CCIFG;	//Reset des Interrupt Flags
   }

//Erweiterung der main.c

#include <msp430.h> 
#include <hal_general.h>
#include <hal_gpio.h>

extern ButtonCom button;	//in hal_gpio.h und main.c

void main(void)
{
	
	//....
	
	while(1) {

	    if(button.active == 1) {
	        switch(button.button) {
	        case 6:
	            LCD_BACKLIGHT_ON;
	            break;
	        case 7:
	            LCD_BACKLIGHT_OFF;
	            break;
	        }
	        button.active = 0;
	    }
	}
}
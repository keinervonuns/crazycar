//Erweiterung der hal_gpio.c

extern ButtonCom button;	//in hal_gpio.h und main.c

void HAL_GPIO_Init(void){

    // ...
    
    P1IE  |= START_BUTTON | STOP_BUTTON;    //Interrupt Enable
    
    //Setzt START_BUTTON und STOP_BUTTON als Inputs mit Pullup resistor
    P1REN |= START_BUTTON | STOP_BUTTON;    
    P1OUT |= START_BUTTON | STOP_BUTTON;

    __enable_interrupt();   //Macht das Programm interruptfahig
    
    // ...

}

#pragma vector=PORT1_VECTOR             //Setzt vector auf PORT1
   __interrupt void port_isr(void)      //Interrupt Service Routine
   {
        //Abfrage der Interrupt Flags
        switch(P1IFG) {
            
            case START_BUTTON:      
            //Wenn START_BUTTON einen Interrupt ausgelost hat
            
                button.active = 1;
                button.button = 6;
                P1IFG &= ~START_BUTTON; //Rucksetzen des Interrupt Flags
                break;
               
            case STOP_BUTTON:
                button.active = 1;
                button.button = 7;
                P1IFG &= ~STOP_BUTTON;
                break;
               
            default:
            //Rucksetzen aller Interupt Flags
                
                P1IFG = 0x00;
                break;
       }
   }




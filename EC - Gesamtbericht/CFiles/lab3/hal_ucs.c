#include <msp430.h>

void HAL_UCS_Init()
{
    UCSCTL6 &= ~XT2OFF;
    UCSCTL3 |= SELREF_2;
    UCSCTL4 |= SELA_2;

    while(SFRIFG1 & OFIFG) //Wait until all error flags are cleared and not set again
    {
    UCSCTL7 &= ~(XT2OFFG + DCOFFG + XT1HFOFFG + XT1LFOFFG);
    SFRIFG1 &= ~OFIFG;
    }
    UCSCTL6 &= ~XT2DRIVE_3; //set to 0
    UCSCTL6 |= XT2DRIVE_2;  //set to stage 2
	
	//Set Masterclock Source to XT2
    UCSCTL4 &= ~SELM_7;     //set to 0
    UCSCTL4 |= SELM__XT2CLK; //set to XT2
	
	//Set Submasterclock Source to XT2
    UCSCTL4 &= ~SELS_7;     //set to 0
    UCSCTL4 |= SELS__XT2CLK; //set to XT2
	
	//Submaster Clock Divider: divide by 8
    UCSCTL5 &= ~DIVS__1;
    UCSCTL5 |= DIVS__8;
}

#ifndef __HAL_UCS__
    #define __HAL_UCS__
    #define XTAL_FREQU 20000000
    #define MCLK_FREQU 20000000
    #define SMCLK_FREQU 2500000

    void HAL_UCS_Init();

#endif

/*
PROTOKOLL VERSION

Kommentiert: 11.12.2018
*/

#include <hal_dma.h>
#include <msp430.h>
#include <hal_gpio.h>
#include <hal_adc12.h>

extern ADC12Com ADC_Data; 			// from ADC header

void HAL_DMA_Init(void) {
	
    DMACTL0 |= DMA0TSEL__ADC12IFG ; //ADC12 as Trigger

    DMA0CTL |= DMADT_1;             //Block Transfer Mode
    DMA0CTL |= DMADSTINCR_3;        //Destination increment
    DMA0CTL |= DMASRCINCR_3;        //Source inctement

    DMA0CTL |= DMAIE;               //Interrupt enable

    DMA0SZ = 0x0004;      			//4 Word length
	
	//Source Adress:
    DMA0SA = &ADC12MEM0;            //First Memory adress of ADC12
	
	//Destination Adress:
    DMA0DA = &ADC_Data.ADCBuffer;   /*
		-   Adress of Data array
		-   The values from ADCBuffer were written directly to the display
			in the main file.*/

    DMA0CTL |= DMAEN;
 }

#pragma vector=DMA_VECTOR
   __interrupt void DMA_isr(void)
   {
	   
       ADC_Data.Status.B.ADCrdy = 1;
       ADC12IFG = 0x0000;
       ADC12CTL0 |= ADC12ENC;

       DMA0CTL &= ~DMAIFG;
       DMA0CTL |= DMAEN;

   }

//Function to set the Cursor

void Driver_LCD_SetPosition(unsigned char page, unsigned char col){

    //sets cursor on display
    //"set cursor" is a display-command

	//reads higher halfbyte, 0 for lower bit, 1 for higher bit:
    unsigned char msb_col = (col >> 4) | 0x10;    
	
	//masks col for lower halfbyte:
    unsigned char lsb_col = col & 0x0F;            

    unsigned char LCD_Pos_Array[3]; 	//array contains data for the cursor
    LCD_Pos_Array[0] = 0xB0 | page; 	//B for page command, 0 for the page
    LCD_Pos_Array[1] = msb_col;     	//higher halfbyte
    LCD_Pos_Array[2] = lsb_col;    		//lower halfbyte

    Driver_LCD_WriteCommand(LCD_Pos_Array, 3); 	//fires the command

	//wait for successful transmission of the command:
    while(LCD_Data.Status.B.TxSuc == 0);        
}
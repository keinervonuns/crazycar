//Doku

#include <hal_usciB1.h>
#include <hal_gpio.h>
#include <msp430.h>

extern USCIB1_SPICom spi_status;	//varible comes from hal_general.c

void HAL_USCIB1_Init(void) {
	
    //Control Register 1:
	UCB1CTL1 &= 0x00;
    UCB1CTL1 |= UCSWRST;        //Software Reset enabled (for changes)
    UCB1CTL1 |= UCSSEL__SMCLK;	//Set UCB1 Clock to Submaster Clock

	//Control Register 0:
    UCB1CTL0 &= 0x00;
    UCB1CTL0 |= UCCKPL;			//rising clockedge
    UCB1CTL0 |= UCMSB;			//when transmitting, MSB first
    UCB1CTL0 |= UCMST;			//master mode
	
	//Bit Rate Control Register 0:
    UCB1BR1 = 0x00;
    UCB1BR0 = 0x19;             //25 DEZ
	
    UCB1CTL1 &= ~UCSWRST;		//Software Reset disable (for changes)
	
	//Interupt Enable Register
    UCB1IE = UCRXIE;           	//Recieve Interrupt
}

void HAL_USCIB1_Transmit(void) {
	
	/*
	This function starts an transmission.
	It uses the global variable USCIB1_SPICom spi_status.
	*/
	
    if(spi_status.Status.B.TxSuc == 1) {
        
		//Start condition, no transmission in action
		
		spi_status.TxData.cnt = 0;	// begin with byte 0
		
        if ((spi_status.TxData.cnt + 1) <= spi_status.TxData.len) {
			
			//if the next byte <= length of data
			
            LCD_CS_LOW;								//select LCD display
            UCB1TXBUF = spi_status.TxData.Data[0];	//write byte into buffer
            spi_status.Status.B.TxSuc = 0;			//success=0
        }
    }
}

#pragma vector=USCI_B1_VECTOR
   __interrupt void usci_B1_isr(void)
   {
	   
		/*
		This interrupt is triggered by the LCD when a byte was recieved
		*/
	   
		spi_status.Status.B.TxSuc = 1;
		//indicates a successful transmission	 
		
		spi_status.RxData.Data[spi_status.TxData.cnt] = UCB1RXBUF;
		//Writes the Receive Buffer into Recieved Data
	   
		spi_status.RxData.len += 1;		//Increase the received Word length
		LCD_CS_HIGH;					//unselect LCD display
		
		//In the next step, the next Byte is going to be transmitted

		if ((spi_status.TxData.cnt + 1) < spi_status.TxData.len) {
				LCD_CS_LOW;
				spi_status.TxData.cnt += 1;
				UCB1TXBUF = spi_status.TxData.Data[spi_status.TxData.cnt];
				spi_status.Status.B.TxSuc = 0;
			}
   }
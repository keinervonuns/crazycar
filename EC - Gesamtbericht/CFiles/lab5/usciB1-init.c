//Doku part1

#include <hal_usciB1.h>
#include <hal_gpio.h>
#include <msp430.h>

//...

void HAL_USCIB1_Init(void) {
	
    //Control Register 1:
	UCB1CTL1 &= 0x00;
    UCB1CTL1 |= UCSWRST;        //Software Reset enabled (for changes)
    UCB1CTL1 |= UCSSEL__SMCLK;	//Set UCB1 Clock to Submaster Clock

	//Control Register 0:
    UCB1CTL0 &= 0x00;
    UCB1CTL0 |= UCCKPL;			//rising clockedge
    UCB1CTL0 |= UCMSB;			//when transmitting, MSB first
    UCB1CTL0 |= UCMST;			//master mode
	
	//Bit Rate Control Register 0:
    UCB1BR1 = 0x00;
    UCB1BR0 = 0x19;             //25 DEZ
	
    UCB1CTL1 &= ~UCSWRST;		//Software Reset disable (for changes)
	
	//Interupt Enable Register
    UCB1IE = UCRXIE;           	//Recieve Interrupt
}

//...
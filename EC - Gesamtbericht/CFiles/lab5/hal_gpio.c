#include <hal_gpio.h>
#include <msp430.h>

void HAL_GPIO_Init(void){
	
	//...
	
	//----------------------------------------
    //Port8
	//----------------------------------------
	
    P8SEL |= LCD_SPI_CLK | LCD_SPI_MISO | LCD_SPI_MOSI;
    P8DIR &= (~UART_RXD_AUX)&(~LCD_SPI_MISO);
	
	//...

}
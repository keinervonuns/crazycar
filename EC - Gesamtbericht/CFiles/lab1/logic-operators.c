//Beispiel von Maskierung

#include<msp430.h>      //Zur Verwendung der BITX ausdrucke


//Zum Setzen auf 1 eines Bits, ohne den Zustand der anderen zu verandern
//======================================================================

P1DIR |= BIT0;  

/*
  XXXX XXXX     - Zustand von P1DIR
|=0000 0001     - BIT 0
-----------
  XXXX XXX0     - Neuer Zustand von P1DIR
*/


//Zum Setzen auf 0 eines Bits, ohne den Zustand der anderen zu verandern
//======================================================================

P1DIR &= ~BIT0;

/*
  XXXX XXX1     - Zustand von P1DIR
&=1111 1110     - ~BIT 0
-----------
  XXXX XXX0     - Neuer Zustand von P1DIR
*/

#ifndef __HAL_GPIO__
    #define __HAL_GPIO__

//Port defaults
#define sel_def 0x00     //define all pins as I/O
#define dir_def 0xff     //define all pins as output
#define out_def 0x00     //define all pins as LOW

//Port 1
#define RPM_SENSOR      BIT3    //in
#define RPM_SENSOR_DIR  BIT4    //in
#define I2C_INT_MOTION  BIT5    //out
#define START_BUTTON    BIT6    //in
#define STOP_BUTTON     BIT7    //in

//Port 2
#define DEBUG_TXD       BIT0    //in
#define DEBUG_RXT       BIT1    //out
#define AUX_PIN_1       BIT2
#define AUX_PIN_2       BIT3
#define AUX_PIN_3       BIT4
#define AUX_PIN_4       BIT5
#define I2C_SDA_MOTION  BIT6
#define I2C_SCL_MOTION  BIT7    //out

//Port 3
#define THROTTLE        BIT2    //out
#define STEERING        BIT3    //out
#define SMCLK           BIT4    //
#define DISTANCE_FRONT_END  BIT7//out

//Port 4
#define LINE_FOLLOW_2   BIT3    //in
#define LINE_FOLLOW_1   BIT4    //in
#define LINE_FOLLOW_3   BIT5    //in
#define LINE_FOLLOW_4   BIT6    //in
#define LINE_FOLLOW_5   BIT7    //in

//Port 5
//nothing connected

//Port 6
#define DISTANCE_RIGHT  BIT0    //in
#define DISTANCE_LEFT   BIT1    //in
#define DISTANCE_FRONT  BIT2    //in
#define VBAT_MEASURE    BIT3    //in
#define DISTANCE_LEFT_EN    BIT4    //out

//Port 7
#define XT2IN       BIT0
#define XT2OUT      BIT1

//Port 8
#define LCD_BL      BIT0    //out
#define LCD_SPI_CS  BIT1    //out
#define UART_TXD_AUX    BIT2    //out
#define UART_RXD_AUX    BIT3    //in
#define LCD_SPI_CLK     BIT4    //out
#define LCD_SPI_MOSI    BIT5    //out
#define LCD_SPI_MISO    BIT6    //in
#define LCD_DATACMD     BIT7    //out

//Port 9
#define LCD_RESET       BIT0    //out
#define DISTANCE_RIGHT_EN   BIT7    //

//LCD
#define LCD_BACKLIGHT_ON    (P8OUT |= LCD_BL)
#define LCD_BACKLIGHT_OFF    (P8OUT &= ~LCD_BL)

#endif

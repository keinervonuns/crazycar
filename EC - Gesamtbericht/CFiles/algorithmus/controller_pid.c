#include <controller_pid.h>

extern RPM RPM_Data;
extern distances dist;

PID pid_para_steering;
PID pid_para_throttle;
PIDContr pid_steering;
PIDContr pid_throttle;


void controllerInit(){
    //--------------------steering init--------------------
    //concatinate the two structs
    pid_steering.parameter = pid_para_steering;
    //set the parameters for the steering P-Controller
    pid_steering.parameter.e = 0;
    pid_steering.parameter.kp = 0.5;        //0,8 1500 akku
    pid_steering.parameter.ki = 0.001;
    pid_steering.parameter.kd = 5;
    pid_steering.parameter.ta = 1;
    pid_steering.parameter.satUp = 30;
    pid_steering.parameter.satLow = -30;
    pid_steering.parameter.esum = 0;
    pid_steering.parameter.eold = 0;

    pid_steering.sollwert = 0;
    pid_steering.istwert = 0;
    pid_steering.enabled = 0;
    pid_steering.y = 0;
    pid_steering.rev = 0;

    //--------------------throttle init--------------------
    //concatinate the two structs
    pid_throttle.parameter = pid_para_throttle;
    //set the parameters for the throttle PID-Controller
    pid_throttle.parameter.e = 0;
    pid_throttle.parameter.kp = 1;    //0.06    //5 1500 akku
    pid_throttle.parameter.ki = 0.2; //0.04    //0.09 1500 akku
    pid_throttle.parameter.kd = 0.1; //0.005
    pid_throttle.parameter.ta = 1;
    pid_throttle.parameter.satUp = 150;
    pid_throttle.parameter.satLow = -50;
    pid_throttle.parameter.esum = 0;
    pid_throttle.parameter.eold = 0;

    pid_throttle.sollwert = 0;
    pid_throttle.istwert = 0;
    pid_throttle.enabled = 0;
    pid_throttle.y = 0;
    pid_throttle.rev = 0;
}

void PIDControl(PIDContr* c){

    c->parameter.e = c->sollwert - c->istwert;

    if((c->y > c->parameter.satLow) && (c->y < c->parameter.satUp))
    {
        c->parameter.esum = c->parameter.esum + c->parameter.e;                             //anti-windup
    }

    c->y = (c->parameter.kp * c->parameter.e);                                              //p-part
    c->y += (c->parameter.ki * c->parameter.ta * c->parameter.esum);                        //i-part
    c->y += (c->parameter.kd * (c->parameter.e - c->parameter.eold) / c->parameter.ta);   //d-part
    c->parameter.eold = c->parameter.e;

    if ((c->y < 0) && (c->parameter.satLow == -50)){
        c->y *= 10;
    }

    if(c->y < c->parameter.satLow) {
        c->y = c->parameter.satLow;
    } else if (c->y > c->parameter.satUp) {
        c->y = c->parameter.satUp;
    }

}

//calculate pid and return the speed in timer ticks
int controllerPIDThrottle(){
    pid_throttle.istwert = RPM_Data.speed_rpm / 18;
    if(pid_throttle.enabled == 1){
        PIDControl(&pid_throttle);
        return (3000 + (pid_throttle.y * 10)) * 2.5;
    } else {
        return (3000 + (pid_throttle.sollwert * 10)) * 2.5;
    }

}

//calculate pid and return the steering in %
int controlPIDSteering(){
    pid_steering.istwert = dist.side;
    if(pid_steering.enabled == 1){
        PIDControl(&pid_steering);
        return pid_steering.y*-1;
    } else {
        return 0;
    }
}

//reset the pid parameters
void resetPID(){
    //----------throttle----------
    pid_throttle.parameter.e = 0;
    pid_throttle.parameter.eold = 0;
    pid_throttle.parameter.esum = 0;
    pid_throttle.y = 0;

    //----------steering----------
    pid_steering.parameter.e = 0;
    pid_steering.parameter.eold = 0;
    pid_steering.parameter.esum = 0;
    pid_steering.y = 0;
}


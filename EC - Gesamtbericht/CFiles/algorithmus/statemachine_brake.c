#include <statemachine_brake.h>

extern ButtonCom button;
extern ADC12Com ADC_Data;
extern RPM RPM_Data;
extern PIDContr pid_throttle;
extern PIDContr pid_steering;
extern loopvars loop;

extern int blink;
extern int way_cm_total;

statevars stb;
distances dist;

int straight_falg;
int debug;
int flag;
int straightright;
int failcnt;

//main state machine
void statemachine_brake() {
    
    //set the default PID values
    pid_steering.enabled = 0;
    pid_throttle.enabled = 1;
    pid_throttle.rev = 0;

    //refresh the current cm value
    stb.compare = getWayCmTotal();

    //refresh the distances
    dist.front = getDistance(SENS_FRONT);
    dist.right = getDistance(SENS_RIGHT);
    dist.left = getDistance(SENS_LEFT);
    dist.side = dist.right - dist.left;

    //check if a jump has been detected
    dist.jump_front = isJump(SENS_FRONT, dist.front);
    dist.jump_right = isJump(SENS_RIGHT, dist.right);
    dist.jump_left = isJump(SENS_LEFT, dist.left);

    //check if the STOP Burron is pressed 
    if((stb.mystate != 1) && !(P1IN & STOP_BUTTON)){
        state = INIT;
        resetfailtimer();   //Back to debounce time
    }

    //cycle through the direction modes if the STOP Button 
    // is pressed while in INIT State
    if(button.active == 1) {
        if(button.button == 7) {
            if(stb.mystate == 1) {
               if(stb.direction == RICHTUNG_RECHTS) {
                   stb.direction = RICHTUNG_LINKS;
               } else if(stb.direction == RICHTUNG_LINKS) {
                   stb.direction = RICHTUNG_RECHTS;
               } else {
                   stb.direction = RICHTUNG_LINKS;
               }
               button.active = 0;
       }
    }

    //Main Statemachine
    switch(state) {
        case INIT:
            stb.speeddivider = 3.7;     //set the speed scaling factor
            LCD_BACKLIGHT_ON;
            
            resetRoundaboutDetection();

            //disable all actuators
            pid_throttle.enabled = 0;
            pid_steering.enabled = 0;
            Driver_SetSteeringInit();
            Driver_SetThrottle(0);
            Driver_Reverse(0);

            //switch to the straight state if the Start button is pressed
            if((button.active == 1) && (button.button == 6)) {
                LCD_BACKLIGHT_OFF;
                state = STRAIGHT;
                button.active = 0;
            }

            resetPID();

            initStatevars();        //initialize all the flags

            stb.mystate = 1;
            break;

        //state for driving straight
        case STRAIGHT:
            loop.roundabout = 0;

            //starten des Failtimers
            if(stb.failtime==0){
                startFailTimer();
            }

            //Set the speed factor according to the situation
            if (stb.bowlexit_bridge){
                stb.speeddivider = 4.5;
            } else if (ADC_Data.battery < 2250) {
                stb.speeddivider = 3.7;
            } else {
                stb.speeddivider = 3.7;
            }

            //enable the steering via the PID Controller
            pid_steering.enabled = 1;

            //sets the speed value according to the situation
            ////brakes if is too fast before a turn
            if((dist.front < 150) && (RPM_Data.speed_rpm > BRAKERPM_STRAIGHT)){
               stb.throttle = -40;
               debug = 0;
            ////does not let the throttle go lower than BRAKETO
            } else if (stb.throttle < BRAKETO){     
               stb.throttle = BRAKETO;
               debug = 0;
            ////regulates the throttle accourding to the front sensor and the speed factor
            } else {
               stb.throttle = (dist.front / stb.speeddivider);
               debug = 1;
               if (stb.throttle < 0){
                   stb.throttle = 0;
               }
            }

            Driver_SetThrottle(stb.throttle);

            //decides if and wich turn it should make
            int test = sideDecider();
            switch(test){
                case 1:
                case 2:
                    //sets the necessairy values for a right turn
                    getRight();

                    //if it drove mote than 3.5 meters straight, 
                    // check if it is driving in the right direction
                    if (stb.compare > (stb.straightstart + 350)) {
                        //writes the code for this action to the memory array
                        mem(2);

                        //if the direction is wrong, make a three point turn                       
                        if((stb.direction == RICHTUNG_LINKS) && (dist.front > 50)){
                            state = RPOINT3_1;
                            resetfailtimer();
                        }else{
                            initStatevars();
                        }
                    }

                    //if the front distance at the beginning of the turn is greater 
                    // than 120cm, detect it as a long u-turn
                    if (dist.front > 120){
                        //writes the code for this action to the memory array
                        mem(3);
                        stb.enablelong180 = 1;
                    } else {
                        stb.enablelong180 = 0;
                    }

                    //save the total distance at wich the turn started
                    stb.steerstart = getWayCmTotal();

                    break;
                case 3:
                case 4:
                    //sets the necessairy values for a left turn
                    getLeft();

                    //if it drove mote than 3.5 meters straight, 
                    // check if it is driving in the right direction
                    if (stb.compare > (stb.straightstart + 350)) {
                        if ((stb.direction == RICHTUNG_RECHTS) && (dist.front > 50)) {
                            state = LPOINT3_1;
                            resetfailtimer();
                        } else {
                            initStatevars();
                        }
                    }

                    //change the speed if the flag for the bowlexit 
                    // without the bridge on top is set
                    if (stb.bowlexit_free) {
                        stb.speeddivider = 4;
                    }

                    //if the front distance at the beginning of the turn is greater 
                    // than 120cm, detect it as a long u-turn
                    if (dist.front > 120){
                        stb.enablelong180 = 1;
                        //writes the code for this action to the memory array
                        mem(3);
                    } else {
                        stb.enablelong180 = 0;
                    }

                    //save the total distance at wich the turn started
                    stb.steerstart = getWayCmTotal();

                    break;

                default:
                    //if it is too close to a wall in the front, 
                    // turn in the direction where there is more space
                    if ((dist.front < stb.emdist)) {
                        stb.emturn = 1;
                        if (stb.bowlexit_bridge){
                            getLeft();
                        } else if (dist.side >= 0) {
                            getRight();
                        } else {
                            getLeft();
                        }
                    } else {
                        //enable the steering controller
                        pid_steering.enabled = 1;
                        //and run it once
                        controlSteering();
                    }
                    break;
            }

            //reset the bowlexit flag if on a straight longer than 250cm 
            // (between exit and first u turn in right direction)
            if (stb.compare > (stb.straightstart + 250)) {
                stb.bowlexit_free = 0;
                stb.speeddivider = 4;
            }

            //check if the car is moving after the timer ended
            if (stb.failtime == 2){
                failcheckBack();
            }

            stb.mystate = 2;
            break;
        //state for a right turn
        case RIGHT:

            //if after 2m the state is still RIGHT, we are driving in a circle
            if((stb.compare - stb.steerstart) > 200 ){
                loop.roundabout=1;
            }

            //decides what to do if the car drives in a circle
            roundaboutRightHandler();

            //resets the failtimer if it has ended
            if (stb.failtime == 2){
               stb.failtime = 0;
            }

            //if this turn is an emergency turn (not much space in front), lower the speed
            if (stb.emturn) {
                Driver_SetThrottle(TURNSPEED-15);
            } else {
                Driver_SetThrottle(TURNSPEED);
            }

            //wait a certain way before turning to avoid a wall collision
            if (stb.compare > stb.steerwait){
                Driver_SetSteering(100);
            }

            //decide in wich state to go next
            if ((stb.bowlexit_bridge == 1) && (dist.front < 50)){
                state = KEEPRIGHT;
                resetfailtimer();
            ////detect the left turn out of the bridged bowl exit
            } else if ((stb.bowlexit_bridge == 1) && (dist.jump_left) && ((stb.compare - stb.steerstart) > 80)) {
                state = KEEPRIGHT;
                resetfailtimer();
            ////hold right if in the bowl coming from the u turns
            } else if ((stb.bowlexit_bridge == 1) && (dist.right < 40)) {
                state = KEEPRIGHT;
                resetfailtimer();
            ////detect the bowl exit when coming from the long straight
            } else if ((dist.left > 60) && (dist.right > 60) && (stb.bowlexit_free == 1) && (dist.jump_left) && (stb.compare > (stb.steerstart + 80))){
                getLeft();
            ////get back straight after a turn
            } else if ((dist.front > 115) && (dist.right < 65) && (dist.left < 65)) {
                getStraightRight();
            ////get back straight after the long u-turn where the left sensor might see too far
            } else if (stb.enablelong180 && (dist.front > 199) && (dist.right < 50)){
                getStraightRight();
            //// get back straight if there is definitely a long straight ahead
            } else if (dist.front > 240) {
                getStraightRight();
            }

            //get straight and keep straight for x cm
            if ((dist.front > 150)  && ((stb.compare - stb.steerstart) > 80)){
                getStraightRight();
                flag = 1;
            }

            //if the failtimer is not running and too close to a wall, reverse
            if((dist.front < 20) || (stb.failtime != 1)){
                failcheckBack();
            }

            stb.mystate = 3;
            break;

        //state for a left turn
        case LEFT:

            //if after 2m the state is still RIGHT, we are driving in a circle
            if((stb.compare - stb.steerstart) > 200 ){
                loop.roundabout=1;
            }

            roundaboutLeftHandler();

            //if too close to a wall or standing still, reverse
            if((dist.front < 20) || (RPM_Data.speed_rpm == 0)){
                state = FAILSAVE_BACK;
                resetfailtimer();
                stb.failstart = getWayCmTotal();
                break;
            }
    
            //set the throttle according to the situation
            if (stb.bowlexit_free){
                Driver_SetThrottle(TURNSPEED-15);
            } else if (stb.bowlexit_bridge) {
                Driver_SetThrottle(TURNSPEED-10);
            } else if (stb.emturn){
                Driver_SetThrottle(TURNSPEED-15);
            } else {
                Driver_SetThrottle(TURNSPEED);
            }

            //prevent from steering into the small wall at the bowl exit coming from the u-turns
            if (stb.bowlexit_bridge && (dist.front > 60)){
                Driver_SetSteering(0);
            } else {
                Driver_SetSteering(-100);
            }

            //decide what to do next
            ////get straight after the bowl coming from the long straight
            if ((stb.bowlexit_free == 1) && (dist.front > 200)){
                getStraightLeft();
            ////get straight after a rregulat turn
            } else if ((dist.front > 130) && (dist.right < 65) && (dist.left < 65)) {
                getStraightLeft();
            ////get straight after the long u-turn
            } else if (stb.enablelong180) {
                if((dist.front > 199) && (dist.left < 50)){
                    getStraightLeft();
                }
            ////get back straight if there is definitely a long straight ahead
            } else if (dist.front > 240) {
                getStraightLeft();
            }

            //get straight and keep straight for x cm
            if((dist.front > 150) && ((stb.compare - stb.steerstart) > 80)){
                getStraightLeft(); 
                flag = 1;
            }

            stb.mystate = 4;
            break;

        //state for reversing
        case FAILSAVE_BACK:
           loop.roundabout = 0;

           //start the failtimer
           if(stb.failtime==0){
               startFailTimer();
           }

           //disable the pid controller and set to reverse
            pid_throttle.enabled = 0;
            pid_throttle.rev = 1;

            Driver_SetSteering(0);
            Driver_Reverse(50);

            //get back straight after driving back 30cm
            if((stb.failstart + 30) < stb.compare)  {
                pid_throttle.rev = 0;
                stb.straightlength=10;
                state = STRAIGHT;
                resetfailtimer();
                Driver_SetSteering(0);
                stb.failstart = 0;
            }
           
            //if the failtimer is up, count the fails 
            if(stb.failtime==2){
                failcnt++;
                //if it got stuck driving backwards
                if ((dist.front > 40) || (RPM_Data.speed_rpm == 0)){
                    stb.emturn = 1;
                    //make a forward
                    if (stb.bowlexit_bridge){
                        getLeft();
                    } else if (dist.side >= 0) {
                        getRight();
                    } else {
                        getLeft();
                    }
                }
            }

            //if it failed too often, try to drive straight for 20cm
            if (failcnt > 5) {
                state = KEEPSTRAIGHT;
                resetfailtimer();
                Driver_SetSteering(0);
                stb.keep_cm = getWayCmTotal();
                stb.straightlength = 20;
                stb.failtime=0;
            }

            stb.mystate = 6;
            break;

        //case for getting out of a right circle 
        case LOOP_RIGHT:
            //reverse
            pid_throttle.enabled = 0;
            pid_throttle.rev = 1;

            //check if standing still and go to straight state if so
            failcheckFront();

            Driver_SetSteering(0);
            Driver_Reverse(50);

            //after 30 cm make a foreard left turn
            if((stb.failstart + 30) < stb.compare) {
                pid_throttle.rev = 0;
                getLeft();
            }

            stb.mystate = 7;
            break;

        //case for getting out of a left circle 
        case LOOP_LEFT:
            //reverse
            pid_throttle.enabled = 0;
            pid_throttle.rev = 1;

            //check if standing still and go to straight state if so
            failcheckFront();

            Driver_SetSteering(0);
            Driver_Reverse(50);

            //after 30 cm make a foreard right turn
            if((stb.failstart + 30) < stb.compare)  {
                pid_throttle.rev = 0;
                getRight();
            }

            stb.mystate = 8;
            break;
        //Point one of the 3-Point turn for driving in the "right" direction
        case RPOINT3_1:
            //Drive straight
            Driver_SetSteering(0);
            Driver_SetThrottle(20);

            //check if standing still and go to straight state if so
            failcheckFront();

            //go to step 2 if at the wall
            if(dist.front < 25){
                state = RPOINT3_2;
                resetfailtimer();
            }

            stb.mystate = 9;
            break;

        //Point two of the 3-Point turn for driving in the "right" direction
        case RPOINT3_2:
            //steer right and reverse
            pid_throttle.enabled = 0;
            pid_throttle.rev = 1;

            Driver_SetSteering(100);
            Driver_Reverse(55);

            //check if standing still and go to straight state if so
            failcheckFront();

            //after 50cm make a forward left turn and the 3-Point turn is complete
            if(dist.front > 50) {
                pid_throttle.rev = 0;
                getLeft();
            }

            stb.mystate = 10;
            break;
        //Point one of the 3-Point turn for driving in the "left" direction
        case LPOINT3_1:
            //Drive straight
            Driver_SetSteering(0);
            Driver_SetThrottle(20);

            //check if standing still and go to straight state if so
            failcheckFront();

            //go to step 2 if at the wall
            if(dist.front < 25){
                state = LPOINT3_2;
                resetfailtimer();
            }

            stb.mystate = 11;
            break;
        //Point two of the 3-Point turn for driving in the "left" direction
        case LPOINT3_2:
            //steer left and reverse
            pid_throttle.enabled = 0;
            pid_throttle.rev = 1;

            Driver_SetSteering(-100);
            Driver_Reverse(45);

            //check if standing still and go to straight state if so
            failcheckFront();

            //after 60cm make a forward right turn and the 3-Point turn is complete
            if(dist.front > 60) {
                pid_throttle.rev = 0;
                getRight();
            }

            stb.mystate = 12;
            break;
        
        //drive straight for x cm no matter what
        case KEEPSTRAIGHT:
            //start the failtimer if not already running
            if(stb.failtime==0){
                startFailTimer();
            }

            Driver_SetSteering(0);  //reset the steering

            //drive straight as long as not too close to a wall. if so make a turn.
            if(dist.front > 50){
                if((stb.compare - stb.keep_cm) > stb.straightlength ){
                    state=STRAIGHT;
                    resetfailtimer();
                    stb.keep_cm=0;
                }else{
                    Driver_SetThrottle(60);
                }
            } else {
                if (stb.bowlexit_bridge){
                    getLeft();
                }if (dist.side >= 0) {
                    getRight();
                } else {
                    getLeft();
                }
            }

            //check if need to reverse
            if(stb.failtime==2){
                failcheckBack();
            }

            stb.mystate = 13;
            break;

        //state for driving along the right wall
        case KEEPRIGHT:
            //start the failtimer
            if(stb.failtime==0){
                startFailTimer();
            }

            //calculate the steering value and caps it between +-30
            stb.steer = (dist.right - 20) * 3;

            if (stb.steer < -30){
                stb.steer = -30;
            }

            if (stb.steer > 30){
                stb.steer = 30;
            }

            Driver_SetSteering(stb.steer);

            //make a left turn if jump is detected
            if (dist.jump_left){
                getLeft();
            //make an emergency turn if too close to a wll
            } else {
                if ((dist.front < stb.emdist)) {
                    stb.emturn = 1;
                    if (stb.bowlexit_bridge){
                        getLeft();
                    } else if (dist.side >= 0) {
                        getRight();
                    } else {
                        getLeft();
                    }
                }
            }

            //check if need to reverse
            if(stb.failtime==2){
                failcheckBack();
            }

            stb.mystate = 14;
            break;
    }
    
    //decides what to do if driving in a circle
    roundaboutExitHandler();
    roundaboutStraightExit();

    controlThrottle();      //runs the PID controller for the throttle once
}

//offsets the steering values to drive closer to the right side to make tighter and faster turns
void holdRight(){
    pid_steering.sollwert = -SIDEOFFSET;
    stb.sidemode = 0;
}

//offsets the steering values to drive closer to the left side to make tighter and faster turns
void holdLeft(){
    pid_steering.sollwert = SIDEOFFSET;
    stb.sidemode = 1;
}

//sets all the necessairy values to get straight after a left turn
void getStraightLeft(){
    state = STRAIGHT;       //changes state to straight
    resetfailtimer();       //resets the failtimer
    Driver_SetSteering(0);  //reset the steering

    //resets all flags after the bowl to avoid flag confusions
    if (stb.bowlexit_bridge) {
        initStatevars();
    }

    mem(0);     //writes the code for this state to the memory array

    //if the flag has been set, keep straight for 50 cm instead of going into the straight state
    if (flag) {
        state = KEEPSTRAIGHT;
        resetfailtimer();
        Driver_SetSteering(0);
        stb.keep_cm = getWayCmTotal();
        stb.straightlength = 50;
        stb.failtime=0;
        flag = 0;
    }

    //set all the necessairy flags and variables
    stb.emturn = 0;
    stb.enablelong180 = 0;
    stb.bowlexit_free = 0;
    stb.speeddivider = 3.7;
    stb.bowlexit_bridge = 0;
    stb.straightstart = getWayCmTotal();

    //decide if the car should hold right or left to take the next turn faster
    if (((stb.compare - stb.steerstart) > 100) && (stb.bowlexit_free == 0)) {
        holdLeft();
    } else {
        holdRight();
    }
}

//sets all the necessairy values to get straight after a right turn
void getStraightRight(){
    state = STRAIGHT;       //changes state to straight
    resetfailtimer();       //resets the failtimer
    Driver_SetSteering(0);  //reset the steering

    //if it was a standard right turn
    if (stb.enablelong180 == 0){
        mem(1);     //write the code for this action to the memory array
    }

    //set all the necessairy flags and variables
    stb.emturn = 0;
    stb.speeddivider = 3.7;
    stb.enablelong180 = 0;
    stb.straightstart = getWayCmTotal();

    //enable the bowl exit when coming from the u-turns
    if (((stb.compare - stb.steerstart) > 100) && (stb.enablelong180 == 0))  {
        stb.bowlexit_bridge = 1;
    } 

    //if the flag has been set, keep straight for 50 cm instead of going into the straight state
    if (flag) {
        state = KEEPSTRAIGHT;
        resetfailtimer();
        Driver_SetSteering(0);
        stb.keep_cm = getWayCmTotal();
        stb.straightlength = 50;
        stb.failtime=0;
        flag = 0;
    }

    
    holdLeft();     //hold left to take the next turn faster
}

//sets all the flags to its default values
void initStatevars(){
    stb.steerwait = 0;
    stb.enablelong180 = 0;
    stb.steer = 0;
    stb.compare = 0;
    stb.sidemode = 0;
    stb.steerstart = 0;
    stb.bowlexit_bridge = 0;
    stb.bowlexit_free = 0;
    stb.straightstart = 0;
    stb.failstart = 0;
    stb.emturn = 0;
    stb.emdist = 60;
}

//starts the failtimer with all the necessairy settings
void startFailTimer(){
    //this is not allowed in state 1(init) because the timer is used to debounce the 
    // stop button to select the driving direction
    if (stb.mystate != 1){
        TA2CCR0 = 10000;
        stb.failtime=1;
        TA2R = 0x0000;           // reset timer A2 register
        TA2CTL |= MC_1;          // set timer A2 to count UP
    }
}

//checks if it is stuck and goes to reverse state if so
void failcheckBack(){
    if(RPM_Data.speed_rpm == 0){
        state = FAILSAVE_BACK;
        resetfailtimer();
        stb.failstart = getWayCmTotal();
        stb.failtime=0;
    }
}

//checks if it is stuck and goes to straight state if so
void failcheckFront(){
    if(RPM_Data.speed_rpm == 0){
        state = STRAIGHT;
        resetfailtimer();
        Driver_SetSteering(0);
        stb.keep_cm = getWayCmTotal();
        stb.failtime=0;
    }
}

//handles a small memory to change some values depending on the recorded turn pattern
void mem(int val){
    stb.mem[2] = stb.mem[1];
    stb.mem[1] = stb.mem[0];
    stb.mem[0] = val;

    //delays the emergrncy turn when driving into the bowl from the long straight
    if (((stb.mem[0] == 1) && (stb.mem[1] == 1) && (stb.mem[2] == 2)) || (stb.mem[1] == 3)){
        stb.emdist = 40;
    } else {
        stb.emdist = 60;
    }

    //activates the bowl exit coming from the long straight after two right turns
    if ((stb.mem[0] == 1) && (stb.mem[1] == 1)){
        stb.bowlexit_free = 1;
    }

}

//resets the failtimer to work for debouncing the stop button
void resetfailtimer() {
    TA2CTL &= ~MC_1;         // stops timer A2 by unsetting count-up
    TA2R = 0x0000;           // reset timer register
    TA2CCR0 = 1000;
    stb.failtime=0;
}

//takes the necessairy actions before taking a right turn
void getRight() {
    state = RIGHT;                      //set the state to right
    startFailTimer();                   //start the failtimer
    stb.steerwait = getWayCmTotal();    //sets the startpoint of the turn
    Driver_SetSteering(100);            //sets the steering hard right
}

//takes the necessairy actions before taking a left turn
void getLeft() {
    state = LEFT;                       //set the state to left
    startFailTimer();                   //start the failtimer
    stb.steerwait = getWayCmTotal();    //sets the startpoint of the turn
    Driver_SetSteering(-100);           //sets the steering hard left
}



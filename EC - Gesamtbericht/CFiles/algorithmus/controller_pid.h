
#ifndef __CONTROLLER_PID__
    #define __CONTROLLER_PID__

    #include <hal_timerA0.h>
    #include <configuration.h>

	//struct for the PID parameters
    typedef struct
    {
        float e;
        float kp;
        float ki;
        float kd;
        float fkp;
        float fki;
        float fkd;
        float rkp;
        float rki;
        float rkd;
        float ta;
        float satUp;
        float satLow;
        float esum;
        float eold;
    } PID;

	//struct for the top level PID values
    typedef struct
    {
        int enabled;
        int rev;
        float sollwert;
        float istwert;
        float y;
        PID parameter;
    }PIDContr;

    void controllerInit();			//sets the values for the two controllers
    int controllerPIDThrottle();	//executes the pid controller once for throttle control
    int controlPIDSteering();		//executes the pid controller once for steering control
    void resetPID();				//resets the regulating difference, the sum, the previous one and the output
#endif

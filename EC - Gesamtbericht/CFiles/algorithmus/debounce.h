/*
 * changelog:
 *
 * 23. Jan 2019:
 * =============
 * created
 *
 */

#ifndef __DEBOUNCE__
    #define __DEBOUNCE__

    #include <msp430.h>
    #include <hal_gpio.h>
    #include <configuration.h>

     void HW_Debounce_Init();

#endif

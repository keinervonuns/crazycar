#ifndef __STSTE_BRAKE__
    #define __STSTE_BRAKE__

    #include <msp430.h>
    #include <configuration.h>
    #include <driver_lcd.h>
    #include <hal_timerA0.h>
    #include <driver_aktorik.h>
    #include <driver_distance.h>
    #include <hal_adc12.h>
    #include <hal_gpio.h>
    #include <jump_detection.h>
    #include <state_methods.h>
    #include <controller_pid.h>
    #include <loop_detection.h>
    #include <jump_detection2.h>

    //external functions
    void statemachine_brake();  //runs the statemachine once

    //internally used functions
    void getStraightRight();    
    void getStraightLeft();
    void initStatevars();
    void blink_lcd();
    void failcheckBack();
    void failcheckFront();
    void mem();
    void startFailTimer();
    void resetfailtimer();
    void getLeft();
    void getRight();
#endif
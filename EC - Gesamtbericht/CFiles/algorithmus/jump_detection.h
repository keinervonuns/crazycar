
#ifndef __JUMP_DETECTION__
    #define __JUMP_DETECTION__

	//settings
    #define QUEUESIZE_FFRONT 10		//size of the shift-register array
    #define QUEUESIZE_RIGHT 5		//size of the shift-register array
    #define QUEUESIZE_LEFT 5		//size of the shift-register array
    #define JUMP_TOLERANCE 25		//min value that counts as jump in cm
    #define TRIGGER_NUMBER 2		//number of detected jumps to count as real jump

	//external function
    int isJump(int sensor, int value);	
	////sensor: 1-front, 2-right, 3-left
	////value: current sensor value
	////return: 1 if a jump on the specified sensor has been detected, else 0

	//internal only
    int getJump(int* history, int size);
    void addQueue(int* queue, int* history, int new_val, int size);
    void addHistory(int* history, int val, int size);
    void clearHistory(int* history, int size);

#endif

#include <state_methods.h>

extern RPM RPM_Data;
extern distances dist;
extern int steering_value;

//decides wich turn to take
int sideDecider(){
    return jumpSide();
}

//deciding based on jump detection and side distance
int jumpSide(){
    //right turn
    if ((dist.jump_right == 1) && (dist.right > 95)){
        //with braking
        if(RPM_Data.speed_rpm > BRAKERPM_TURN){
            return 2;
        //without braking
        } else return 1;
    }

    //left turn
    if ((dist.jump_left == 1) && (dist.left > 95)){
        //with braking
        if(RPM_Data.speed_rpm > BRAKERPM_TURN){
            return 4;
        //without braking
        } else return 3;
    }

    return 0;
}
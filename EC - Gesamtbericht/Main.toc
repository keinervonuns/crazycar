\select@language {ngerman}
\contentsline {section}{\numberline {1}Vorwort}{1}
\contentsline {section}{\numberline {2}Crazy Car Platine\IeC {\textendash }Einf\IeC {\"u}hrung}{2}
\contentsline {subsection}{\numberline {2.1}Das Crazy Car}{2}
\contentsline {subsubsection}{\numberline {2.1.1}Der Mikrocontroller}{2}
\contentsline {subsection}{\numberline {2.2}Software}{2}
\contentsline {subsubsection}{\numberline {2.2.1}Abbildung im Code Composer}{3}
\contentsline {section}{\numberline {3}IO-Port Konfiguration}{4}
\contentsline {subsection}{\numberline {3.1}Pineigenschaften MSP430}{4}
\contentsline {subsection}{\numberline {3.2}IO Konfiguration im Code Composer}{4}
\contentsline {subsubsection}{\numberline {3.2.1}Maskierung}{5}
\contentsline {subsubsection}{\numberline {3.2.2}I-O Konfiguration f\IeC {\"u}r das crazy car}{6}
\contentsline {section}{\numberline {4}Rechendauer Performance Test}{7}
\contentsline {subsection}{\numberline {4.1}Code}{7}
\contentsline {subsection}{\numberline {4.2}Messung am Oszilloskop}{8}
\contentsline {subsubsection}{\numberline {4.2.1}Screens}{8}
\contentsline {subsubsection}{\numberline {4.2.2}Auswertung}{9}
\contentsline {section}{\numberline {5}Allgemeine Interrupt Steuerung}{10}
\contentsline {subsection}{\numberline {5.1}Einstellungen f\IeC {\"u}r die Interruptfunktion}{10}
\contentsline {subsubsection}{\numberline {5.1.1}\#pragma}{10}
\contentsline {subsection}{\numberline {5.2}Interrupt Vector Table}{10}
\contentsline {subsection}{\numberline {5.3}Beispiel: Hintergrundleuchten}{11}
\contentsline {subsubsection}{\numberline {5.3.1}Registereinstellungen f\IeC {\"u}r die Taster}{11}
\contentsline {subsubsection}{\numberline {5.3.2}Code}{12}
\contentsline {subsection}{\numberline {5.4}Interrupt Flag Register}{13}
\contentsline {subsection}{\numberline {5.5}Zus\IeC {\"a}tzlich bearbeitete Themen im Zuge dieses Abschnitts}{13}
\contentsline {subsubsection}{\numberline {5.5.1}Kontaktprellen}{13}
\contentsline {subsubsection}{\numberline {5.5.2}Interrupts und der zeitliche Programmblauf}{14}
\contentsline {subsubsection}{\numberline {5.5.3}Interrupt Nesting}{14}
\contentsline {section}{\numberline {6}Unified Clock System}{15}
\contentsline {subsection}{\numberline {6.1}Konfiguration der Master- und Submaster Clock}{15}
\contentsline {subsubsection}{\numberline {6.1.1}\IeC {\"U}bersicht \IeC {\"u}ber die Frequenzen}{16}
\contentsline {subsubsection}{\numberline {6.1.2}Einstellen der Clock}{17}
\contentsline {subsubsection}{\numberline {6.1.3}Messen der Submasterclock mit dem Oszilloskop}{17}
\contentsline {section}{\numberline {7}Timer}{18}
\contentsline {subsection}{\numberline {7.1}Timer A}{18}
\contentsline {subsection}{\numberline {7.2}Timer B}{18}
\contentsline {subsection}{\numberline {7.3}Unterschied zwischen Timer A und Timer B}{19}
\contentsline {subsection}{\numberline {7.4}Capture- und Comparemode}{19}
\contentsline {subsubsection}{\numberline {7.4.1}Compare Mode}{19}
\contentsline {subsubsection}{\numberline {7.4.2}Capture mode}{20}
\contentsline {subsection}{\numberline {7.5}Z\IeC {\"a}hlarten}{20}
\contentsline {subsubsection}{\numberline {7.5.1}Up-Mode}{20}
\contentsline {subsection}{\numberline {7.6}Beispiel: Display durch Timer B blinken lassen}{21}
\contentsline {subsubsection}{\numberline {7.6.1}Berechnung}{21}
\contentsline {subsubsection}{\numberline {7.6.2}Umsetzung im Codecomposer}{22}
\contentsline {subsubsection}{\numberline {7.6.3}Messung der Blinkzeit mit dem Oszilloskop}{23}
\contentsline {section}{\numberline {8}Ansteuerung des Lenkservos am Crazy Car}{24}
\contentsline {subsection}{\numberline {8.1}PWM Signal f\IeC {\"u}r Lenkung durch TimerA}{24}
\contentsline {subsubsection}{\numberline {8.1.1}PWM durch Up Mode}{25}
\contentsline {subsubsection}{\numberline {8.1.2}Berechnung}{26}
\contentsline {subsubsection}{\numberline {8.1.3}Umsetzung im Code Composer}{26}
\contentsline {subsection}{\numberline {8.2}Zusammenfassung der Lenkeigenschaften am Crazy Car}{27}
\contentsline {section}{\numberline {9}Der Fahrtenregler}{28}
\contentsline {subsection}{\numberline {9.1}Initialisierung des Motors}{28}
\contentsline {subsubsection}{\numberline {9.1.1}Konfiguration f\IeC {\"u}r PWM Modus}{28}
\contentsline {subsubsection}{\numberline {9.1.2}Umsetzung im Code Composer}{30}
\contentsline {section}{\numberline {10}LCD Display}{32}
\contentsline {subsection}{\numberline {10.1}SPI}{33}
\contentsline {subsection}{\numberline {10.2}USCI}{34}
\contentsline {subsubsection}{\numberline {10.2.1}Daten\IeC {\"u}bertragung mit USCI}{34}
\contentsline {subsection}{\numberline {10.3}Kommunikation mit dem Display}{34}
\contentsline {subsubsection}{\numberline {10.3.1}Daten\IeC {\"u}bertragung mit USCI im Codecomposer}{35}
\contentsline {subsubsection}{\numberline {10.3.2}Initialisieren der USCI Schnittstelle}{35}
\contentsline {subsubsection}{\numberline {10.3.3}Das richtige Datenformat}{36}
\contentsline {subsubsection}{\numberline {10.3.4}Cursorposition setzen}{37}
\contentsline {subsubsection}{\numberline {10.3.5}Auf das Display Schreiben}{38}
\contentsline {subsubsection}{\numberline {10.3.6}Exemplarische Messung einer Daten\IeC {\"u}bertragung}{39}
\contentsline {section}{\numberline {11}ADC}{41}
\contentsline {subsection}{\numberline {11.1}Umsetzung am Crazycar}{42}
\contentsline {subsubsection}{\numberline {11.1.1}Konfiguration des ADC}{43}
\contentsline {subsubsection}{\numberline {11.1.2}Starten des ADCs}{45}
\contentsline {section}{\numberline {12}DMA controller}{46}
\contentsline {subsection}{\numberline {12.1}Transferarten}{46}
\contentsline {subsection}{\numberline {12.2}Verwendung am Crazy Car}{47}
\contentsline {section}{\numberline {13}Sharp Abstandssensoren}{48}
\contentsline {subsection}{\numberline {13.1}Funktionsweise}{48}
\contentsline {subsection}{\numberline {13.2}Messung}{49}
\contentsline {subsubsection}{\numberline {13.2.1}Grafische Darstellung der Ausgangsspannung}{50}
\contentsline {subsubsection}{\numberline {13.2.2}Messwerte Tabellen}{51}
\contentsline {subsection}{\numberline {13.3}Auswertung der Distanz am Crazy Car}{52}
\contentsline {section}{\numberline {14}Algorithmus}{53}
\contentsline {subsection}{\numberline {14.1}PID Regler}{53}
\contentsline {subsubsection}{\numberline {14.1.1}controller\_pid.h}{53}
\contentsline {subsubsection}{\numberline {14.1.2}controller\_pid.c}{54}
\contentsline {subsection}{\numberline {14.2}Kurvenerkennung}{56}
\contentsline {subsubsection}{\numberline {14.2.1}jump\_detection.h}{56}
\contentsline {subsubsection}{\numberline {14.2.2}jump\_detection.c}{56}
\contentsline {subsection}{\numberline {14.3}Statemachine}{58}
\contentsline {subsubsection}{\numberline {14.3.1}Failtimer}{58}
\contentsline {subsubsection}{\numberline {14.3.2}statemachine\_brake.h}{58}
\contentsline {subsubsection}{\numberline {14.3.3}statemachine\_brake.c}{59}
\contentsline {subsubsection}{\numberline {14.3.4}state\_methods.h}{71}
\contentsline {subsubsection}{\numberline {14.3.5}state\_methods.c}{72}
\contentsline {subsection}{\numberline {14.4}Entprellen}{72}
\contentsline {subsubsection}{\numberline {14.4.1}debounce.h}{72}
\contentsline {subsubsection}{\numberline {14.4.2}debounce.c}{73}
\contentsline {section}{\numberline {15}Verwendete Logikzeichen}{75}
\contentsline {section}{\numberline {16}Richtigkeitsbeglaubigung}{76}

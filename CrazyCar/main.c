#include <main.h>

extern ButtonCom button;
extern USCIB1_SPICom LCD_Data;
extern ADC12Com ADC_Data;
extern RPM RPM_Data;

void main(void)
{
    HAL_Wdt_Init();
	HAL_Init();                 // Initializes the hardware layer
	Driver_Init();              // Initializes the driver layer
	controllerInit();
	initStatevars();

	while(1) {
	    getRPM();
        refreshDisplay();
        statemachine_brake();
	}
}

#include <hal_general.h>

ButtonCom button;
USCIB1_SPICom LCD_Data;
ADC12Com ADC_Data;
RPM RPM_Data;

void HAL_Init(){
    HAL_Wdt_Init();
    HAL_PMM_Init();
    HAL_GPIO_Init();
    HAL_UCS_Init();
    HAL_ADC12_Init();
    HAL_TimerB0_Init();
    HAL_TimerA1_Init();
    HAL_TimerA0_Init();
    HAL_USCIB1_Init();
    HAL_DMA_Init();

    HW_Debounce_Init();

    __enable_interrupt();
}


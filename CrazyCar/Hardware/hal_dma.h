#ifndef __HAL_DMA__
    #define __HAL_DMA__

    #include <msp430.h>
    #include <hal_gpio.h>
    #include <hal_adc12.h>

    void HAL_DMA_Init(void);

#endif

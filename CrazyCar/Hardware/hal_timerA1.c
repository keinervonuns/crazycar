#include <hal_timerA1.h>

void HAL_TimerA1_Init() {

    TA1CTL &= 0x0000;
    TA1CTL |= TASSEL__SMCLK;        //set clock soure to submaster
    TA1CTL |= MC__UP;

    //TA1R &= 0x0000;               //sets counter to 0
    TA1CCR0 = PERIODA1;

    TA1CCTL1 &= 0x0000;             //set to 0
    TA1CCTL1 |= OUTMOD_7;           //reset/set
    TA1CCR1 = 0;

    TA1CCTL0 &= 0x0000;
    TA1CCTL0 |= CCIE;

    TA1CCTL2 &= 0x0000;
    TA1CCTL2 |= OUTMOD_7;
    TA1CCR2 = PWM_INIT;
}

#ifndef __HAL_ADC12__
    #define __HAL_ADC12__

    void HAL_ADC12_Init(void);

    typedef struct {
        union{
            unsigned char R;
            struct {
                unsigned char ADCrdy:1; // Bit=1 wenn Daten übertragen wurden
                unsigned char dummy:7;
            }B;
        }Status;
        unsigned short ADCBuffer[4];
        unsigned short left;
        unsigned short right;
        unsigned short front;
        unsigned short battery;
    }ADC12Com;


#endif

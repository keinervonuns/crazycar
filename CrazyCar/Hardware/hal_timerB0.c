#include <hal_timerB0.h>

int cntr;

void HAL_TimerB0_Init() {

    TB0CTL  &= 0x0000;          //set to 0
                                //CNTL=0    ... 16 Bit
    TB0CTL  |= TBSSEL__SMCLK;   //Source=Subclock   2.5MHz
    //TB0CTL  |= ID__8;           //Input Divider auf 1/8
    TB0CTL  |= MC__UP;          //UP Mode to TBXCL0

    TB0CCTL0 &= 0x0000;          //set to 0
                                //CAP = Compare Mode
    TB0CCTL0 |= CCIE;           //Interrupt enable
                                //Auslesen mit CCIFG
    TB0EX0  &= 0x0000;          //Set to 0
    //TB0EX0  |= TBIDEX__3;       //Dritteln

    TB0CCR0 = 20833;           //52083
}

#pragma vector=TIMERB0_VECTOR
   __interrupt void timerb0_isr(void)
   {
       ADC12CTL0 |= 0x0001;

       TB0CCTL0 &= ~CCIFG;
   }

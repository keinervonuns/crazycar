#ifndef __HAL_USCIB1__
    #define __HAL_USCIB1__

    #include <msp430.h>
    #include <hal_gpio.h>
    #include <hal_adc12.h>

    void HAL_USCIB1_Transmit(void);
    void HAL_USCIB1_Init();

    typedef struct {
            union{
                unsigned char R;
                struct {
                    unsigned char TxSuc:1; // Bit=1 wenn Daten �bertragen wurden
                    unsigned char dummy:7;
                }B;
            }Status;
                struct {
                    unsigned char len; // L�nge der Daten in Bytes die �bertragen werden
                    unsigned char cnt; // Index auf momentan �bertragene Daten
                    unsigned char Data[256]; // Tx Daten Array
            }TxData;
                struct {
                    unsigned char len; // L�nge der empfangenen Daten
                    unsigned char Data[256]; // Rx Daten Array
            }RxData;
        }USCIB1_SPICom;

#endif

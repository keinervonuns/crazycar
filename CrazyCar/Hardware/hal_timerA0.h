#ifndef __HAL_TIMERA0__
    #define __HAL_TIMERA0__

    #include <msp430.h>
    #include <hal_gpio.h>
    #define PERIOD 25.6

    void HAL_TimerA0_Init();

    //void updateRPM();
    int getRPM();
    int getTurns();
    int getWayCm();
    int getWayM();
    int getWayCmTotal();

    typedef struct {
            unsigned short counter;
            unsigned short speed_rpm;
            int enabled;
        }RPM;
#endif

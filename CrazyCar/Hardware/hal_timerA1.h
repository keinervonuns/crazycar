#ifndef __HAL_TIMERA1__
    #define __HAL_TIMERA1__

    #include <msp430.h>
    #include <hal_gpio.h>

    #define PERIODA1        53750   //period 60Hz
    #define PWM_INIT        3800    //1,5ms -> wheels are straight
    #define SERVO_MAX       4200    //far right
    #define SERVO_MIN       2200    //far left
    #define SERVO_MID       3200    //center

    void HAL_TimerA1_Init();

#endif

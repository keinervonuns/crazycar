#include <hal_adc12.h>
#include <msp430.h>
#include <hal_gpio.h>

extern ADC12Com ADC_Data;

void HAL_ADC12_Init(void){
    ADC12CTL0 &= ~ADC12ENC;

    //Referenzspannung
    REFCTL0 |= REFOUT;
    REFCTL0 |= REFON;
    REFCTL0 &= ~REFMSTR;

    ADC12CTL0 |= ADC12SHT0_15;
    ADC12CTL0 |= ADC12SHT1_15;
    ADC12CTL0 |= ADC12MSC;
    ADC12CTL0 |= ADC12REF2_5V;
    ADC12CTL0 |= ADC12REFON;


    ADC12CTL1 |= ADC12SHS_0;        //TimerB0 starter
    ADC12CTL1 |= ADC12SHP;        //sampling timer
    ADC12CTL1 |= ADC12SSEL_3;       //SMCLK 2.5M
    ADC12CTL1 |= ADC12CONSEQ_1;     //Sequence of channels

    ADC12CTL2 |= ADC12RES_2;        //12Bit Resolution

    //Distnace Right
    ADC12MCTL0 |= ADC12SREF_1;      //VeREF bis AVSS
    ADC12MCTL0 |= ADC12INCH_0;

    //Distance Left
    ADC12MCTL1 |= ADC12SREF_1;      //VeREF bis AVSS
    ADC12MCTL1 |= ADC12INCH_1;

    //Distance Front
    ADC12MCTL2 |= ADC12SREF_1;      //VeREF bis AVSS
    ADC12MCTL2 |= ADC12INCH_2;

    //VBattery
    ADC12MCTL3 |= ADC12SREF_1;      //VeREF bis AVSS
    ADC12MCTL3 |= ADC12EOS;
    ADC12MCTL3 |= ADC12INCH_3;

    //ADC12IE = 0xffff;

    ADC12CTL0 |= ADC12ON;
    ADC12CTL0 |= ADC12ENC;
}

/*
#pragma vector=ADC12_VECTOR
   __interrupt void ADC12_isr(void)
   {
       ADC_Data.ADCBuffer[0] = ADC12MEM0;
       ADC_Data.ADCBuffer[1] = ADC12MEM1;
       ADC_Data.ADCBuffer[2] = ADC12MEM2;
       ADC_Data.ADCBuffer[3] = ADC12MEM3;

       ADC12IFG = 0x0000;

       ADC_Data.Status.B.ADCrdy = 1;

       ADC12CTL0 |= ADC12ENC;
   }
*/

#ifndef __HAL_TIMERB0__
    #define __HAL_TIMERB0__

    #include <msp430.h>
    #include <hal_gpio.h>

    void HAL_TimerB0_Init();

#endif

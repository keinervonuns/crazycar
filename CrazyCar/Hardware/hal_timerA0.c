#include <hal_timerA0.h>

extern RPM RPM_Data;    //from hal_timera0.h

int turn_counter;
int magnet_pass_counter;
int way_cm;
int way_m;
int way_cm_total;
unsigned long time;
unsigned long multi;

void HAL_TimerA0_Init() {
    TA0CTL &= 0x0000;
    TA0CTL |= TASSEL__SMCLK;        //set clock soure to submaster = 2,5MHz
    TA0CTL |= ID__8;                //Divided by 8 = 312,5 kHz
    TA0CTL |= MC__CONTINUOUS;
    TA0CTL |= TAIE;

    TA0CCTL2 &= 0x0000;             //set to 0
    TA0CCTL2 |= CM_1;               //rising edge
    TA0CCTL2 |= SCS;                //sync capture
    TA0CCTL2 |= CAP;                //capture mode
    TA0CCTL2 |= CCIE;
    TA0CCTL2 |= CCIS_0;             //P1.3 as source = RPM sensor

    TA0EX0 = TAIDEX_7;              //Divided by 8 = 39kHz
}

//the full function is RPM_Data.speed_rpm = 60M / (RPM_Data.counter * PERIOD * 11) but it is split because of data type limitations
int getRPM(){
    if (RPM_Data.enabled == 1){
        time = RPM_Data.counter * PERIOD;
        multi = 600000 / time;
        RPM_Data.speed_rpm = multi * 100 / 11;
        return RPM_Data.speed_rpm;
    } else {
        RPM_Data.speed_rpm = 0;
        return RPM_Data.speed_rpm;
    }
}

int getSpeedPercent(){
    if (RPM_Data.counter < 100){
        return 100;

    }else if (RPM_Data.counter > 2000){
        return 0;

    }else{
        return 104-(0.052*RPM_Data.counter);
    }
}

int getTurns(){
    return turn_counter;
}

int getWayCm(){
    return way_cm;
}

int getWayM(){
    return way_m;
}

int getWayCmTotal(){
    return way_cm_total;
}

#pragma vector=TIMER0_A1_VECTOR
   __interrupt void timer_A0_isr(void)
   {
       if (TA0IV == 0x0000000E){
           RPM_Data.enabled = 0;
           TA0IV = 0;
       } else {
           RPM_Data.enabled = 1;
           RPM_Data.counter = TA0R;
           way_cm = way_cm + 1;
           way_cm_total++;

           if(way_cm >= 100){
               way_cm = way_cm - 100;
               way_m = way_m +1;
           }

           //P8OUT ^= LCD_BL;
           TA0CTL |= TACLR;
           TA0R = 0x0000;
           TA0CCTL2 &= ~COV;        //reset overflow
           TA0CCTL2 &= ~CCIFG;      //reset flag
       }
   }

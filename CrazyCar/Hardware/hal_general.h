#ifndef __HAL_GENERAL__
    #define __HAL_GENERAL__

    #include <hal_pmm.h>
    #include <hal_wdt.h>
    #include <hal_gpio.h>
    #include <hal_ucs.h>
    #include <hal_timerB0.h>
    #include <hal_timerA1.h>
    #include <hal_usciB1.h>
    #include <hal_timerA0.h>
    #include <hal_adc12.h>
    #include <hal_dma.h>
    #include <debounce.h>

    void HAL_Init();

#endif

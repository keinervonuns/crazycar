#include <hal_dma.h>

int blink;

extern ADC12Com ADC_Data;

void HAL_DMA_Init(void) {
    DMACTL0 |= DMA0TSEL__ADC12IFG ; //ADC12 as Trigger

    DMA0CTL |= DMADT_1;             //Block Transfer Mode
    DMA0CTL |= DMADSTINCR_3;        //Destination increment
    DMA0CTL |= DMASRCINCR_3;        //Source inctement

    DMA0CTL |= DMAIE;               //Interrupt enable

    DMA0SZ = 0x0004;               //4 Word length

    DMA0SA = &ADC12MEM0;            //First Memory adress of ADC12

    DMA0DA = &ADC_Data.ADCBuffer;   //Adress of Data array

    DMA0CTL |= DMAEN;
 }

#pragma vector=DMA_VECTOR
   __interrupt void DMA_isr(void)
   {

       if (blink){
           blink = 0;
       } else {
           blink = 1;
       }

       ADC_Data.Status.B.ADCrdy = 1;
       ADC12IFG = 0x0000;
       ADC12CTL0 |= ADC12ENC;

       DMA0CTL &= ~DMAIFG;
       DMA0CTL |= DMAEN;

   }

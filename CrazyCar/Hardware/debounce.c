/*
 * changelog:
 *
 * 23. Jan 2019:
 * =============
 * created
 *
 */

#include <debounce.h>

extern ButtonCom button;
extern statevars stb;

void HW_Debounce_Init() {

    // this inits timer A2. timer A2 is used for debouncing the knobs.

    // timer control register
    TA2CTL &= 0x0000;           // set register to 0 - for further settings
                                // -> Compare Mode
    TA2CTL |= TASSEL__SMCLK;    // submaster as clock
    TA2CTL |= ID__8;            // divide clock by 8
    TA2CCR0 = 1000;             // debounce time

    // timer CCR0 control
    TA2CCTL0    &= 0x0000;      // set register to 0 - for further settings
    TA2CCTL0    |= CCIE;        // enable interrupt
    TA2EX0      = TAIDEX_7;     // divide clock by 8

}

#pragma vector=TIMER2_A0_VECTOR

   __interrupt void timera2_isr(void)
   {

       //LCD_BACKLIGHT_OFF;       // backlight for debug
       if(stb.failtime==1){
           //FAIL happened

           stb.failtime=2;  //Phase 2

       } else if(stb.blinklcd){

           stb.blinklcd=0;
           //LCD_BACKLIGHT_OFF;
       }else{

           if(button.debounce==1){

                    // START button

                    if (P1IN & START_BUTTON){

                        //UPPED
                        button.active = 0;
                        button.button = 0;

                    }else{

                        //PRESSED
                        button.active = 1;
                        button.button = 6;

                    }

                }

                if(button.debounce==2){

                    // STOP button

                    if (P1IN & STOP_BUTTON){

                        // stop button UP
                        button.active = 0;
                        button.button = 0;

                    }else{

                        // stop button PRESSED
                        button.active = 1;
                        button.button = 7;
                    }

                }

       }

       TA2CTL &= ~MC_1;         // stops timer A2 by unsetting count-up
       TA2R = 0x0000;           // reset timer register
       TA2CCR0 = 1000;  //set back to debounce time
       TA2CCTL0 &= ~CCIFG;      // unset Interrupt flag

   }

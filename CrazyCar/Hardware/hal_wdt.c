#include <hal_wdt.h>

void HAL_Wdt_Init() {
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timers
}

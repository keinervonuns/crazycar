
#ifndef __DRIVER_LCD__
    #define __DRIVER_LCD__

    #include <msp430.h>
    #include <hal_usciB1.h>
    #include <hal_gpio.h>

    //LCD commands
    #define CMD_LCD_RESET 0xE2
    #define CMD_LCD_BIAS 0xA3
    #define CMD_ADC_SEL_NORMAL 0xA0
    #define CMD_COMMON_REVERSE 0xC8
    #define CMD_RES_RATIO 0x24
    #define CMD_ELEC_VOL_MODE 0x81
    #define CMD_ELEC_VOL_VALUE 0x0F
    #define CMD_POWER_CONT 0x2F
    #define CMD_DISPLAY_ON 0xAF

    #define LCD_COMMAND (P8OUT &= ~LCD_DATACMD)
    #define LCD_DATA    (P8OUT |= LCD_DATACMD)
    #define LCD_MAX_COLM    128

    #define SET_PAGE 0xB0 //0xB0 f�r erste Page , 0xB1 f�r zweite Page
    #define LAST_PAGE 0xB7
    #define MSB_COL_ADDR 0x10 //0x10, 0x11 ,..,0x1F f�r spalten �ber 16
    #define LSB_COL_ADDR 0x00 //0x01, 0x02 ,..,0x0F f�r spalten bis 15

    void Driver_LCD_WriteCommand(unsigned char*, unsigned char data_length);
    void Driver_LCD_Init();
    void Driver_LCD_Clear(void);
    void Driver_LCD_SetPosition(unsigned char page, unsigned char col);
    void LCD_WriteText(unsigned char *text, unsigned char text_length, unsigned char page, unsigned char col);
    void Driver_LCD_WriteUInt (int num, unsigned char page, unsigned char col);
    void WriteText(unsigned char *text, int num, unsigned char page);
    void WriteTextOnly(unsigned char *text1, unsigned char *text2, unsigned char page);

#endif

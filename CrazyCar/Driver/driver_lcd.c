//Commented on 7.11.2018 11:04
//driver_LCD.c

#include <driver_lcd.h>
#include <font_table.h>

extern USCIB1_SPICom LCD_Data;

void Driver_LCD_WriteCommand(unsigned char *data , unsigned char data_length) {

    //this function transmitts data from an array to the LCD display

    //data_length is length in byte
    //data points on char array

     unsigned char i;                        //running variable
     while(LCD_Data.Status.B.TxSuc == 0);    //wait for ongoing transmissions to end
     LCD_COMMAND;                            //set the LCD system to command mode

     for(i=0; i< data_length; i++){

         LCD_Data.TxData.Data[i] = *data;
         //the data to be transmitted are set to the current position via pointer

         data++;
         //pointer is increased to the next byte, because data is char
     }

     LCD_Data.TxData.len = data_length;        //data_length in bytes
     LCD_Data.TxData.cnt = 0;                //data counter is 0
     HAL_USCIB1_Transmit();                    //start transmission
     while(LCD_Data.Status.B.TxSuc == 0);    //wait for transmission to end

}

void Driver_LCD_Init(){

    //process to init display

    LCD_Data.Status.B.TxSuc = 1;    //inits the LCD status to successful to start further transmissions
    //LCD_BACKLIGHT_ON;                //backlight on for debug purpose
    P9OUT &= ~LCD_RESET;            //reset low
    __delay_cycles(100000);            //wait for some time
    P9OUT |= LCD_RESET;                //set reset

    //Data for init process, onde define is a byte and there are 9 of it
    unsigned char initcmd[9] = { CMD_LCD_RESET, CMD_LCD_BIAS,
                              CMD_ADC_SEL_NORMAL, CMD_COMMON_REVERSE,
                              CMD_RES_RATIO, CMD_ELEC_VOL_MODE,
                              CMD_ELEC_VOL_VALUE, CMD_POWER_CONT,
                              CMD_DISPLAY_ON };

    Driver_LCD_WriteCommand(initcmd, 9);    //write all 9 commands
    while(LCD_Data.Status.B.TxSuc == 0);    //wait for successful transmission

    Driver_LCD_Clear();                        //clear what's on the display
    while(LCD_Data.Status.B.TxSuc == 0);    //wait for successful transmission
}

void Driver_LCD_SetPosition(unsigned char page, unsigned char col){

    //sets cursor on display
    //set cursor is a display-command command

    unsigned char msb_col = (col >> 4) | 0x10;    //reads higher halfbyte, 0 for lower bit, 1 for higher bit
    unsigned char lsb_col = col & 0x0F;            //masks col for lower halfbyte

    unsigned char LCD_Pos_Array[3];                //array contains data for the cursor
    LCD_Pos_Array[0] = 0xB0 | page;                //B for page command, 0 for the page
    LCD_Pos_Array[1] = msb_col;                    //higher halfbyte
    LCD_Pos_Array[2] = lsb_col;                    //lower halfbyte

    Driver_LCD_WriteCommand(LCD_Pos_Array, 3);    //fires the command

    while(LCD_Data.Status.B.TxSuc == 0);        //wait for successful transmission of the command
}

void Driver_LCD_Clear(void) {

    //this function provides data for transmission to clear the display via HAL_USCIB1_Transmit()

    int j = 0x00;    //running variable

    unsigned char LCD_Pos_Array[3] = { 0xB0, 0x10, 0x00 };      //1.page, 1.col

    for(j=0x00; (SET_PAGE+j) <= LAST_PAGE; j++) {

            //goes through all pages (=rows)

            LCD_Pos_Array[0] = SET_PAGE+j;                    //sets the current page
            Driver_LCD_WriteCommand(LCD_Pos_Array , 3);        //fires command

            while(LCD_Data.Status.B.TxSuc == 0);            //wait for successful transmission

            LCD_DATA;                                        //Set LCD to DATA

            int i=0;                                        //running variable
            for(i = 0; i < LCD_MAX_COLM; i++) {
                LCD_Data.TxData.Data[i] = 0x00;                //writes 0x00 to current column
                                                            //for data to be transmittet
            }

            LCD_Data.TxData.len = LCD_MAX_COLM;                //length of data to transmit is Column amount
            HAL_USCIB1_Transmit();                            //start transmission to display
        }
}

void WriteText(unsigned char *text, int num, unsigned char page){

    if(text[4] != 0){
        //ERROR - text length doesnt fit
        text="xLEN";
    }

    LCD_WriteText(text, 4, page, 0);
    Driver_LCD_WriteUInt(num, page, 30);

}

void WriteTextOnly(unsigned char *text1, unsigned char *text2, unsigned char page){

    if(text1[4] != 0){
        //ERROR - text length doesnt fit
        text1="xLEN";
    }



    LCD_WriteText(text1, 4, page, 0);
    LCD_WriteText(text2, 6, page, 30);

}


void LCD_WriteText(unsigned char *text, unsigned char text_length, unsigned char page, unsigned char col) {

    /*
     * In dieser Funktion wird der Text ausgegeben
     */

    unsigned char i, text_length_cnt, col_pos = col;
    unsigned char len = 0;

    Driver_LCD_SetPosition(page, col);
    while(LCD_Data.Status.B.TxSuc == 0);
    LCD_DATA;

    for(text_length_cnt=0; text_length_cnt < text_length; text_length_cnt++) // Textlšnge des Strings abarbeiten
    {
        if(col+len*6 >= 126) {
            col = 0;
            len = 0;
            page++;
            Driver_LCD_SetPosition(page, col);
            LCD_DATA;
        }

        for(i=0; i < fonts_width_max; i++) // Jedes Character des Strings
        {
            LCD_Data.TxData.Data[i] = font_table[*text][i];
            col_pos++;
        }

        LCD_Data.TxData.len = fonts_width_max;
        HAL_USCIB1_Transmit();
        while(LCD_Data.Status.B.TxSuc == 0);

        len++;
        text++;
    }
}

//writes an integer type on the display
void Driver_LCD_WriteUInt (int num, unsigned char page, unsigned char col) {

    int numcpy = num;           //numcpy = copy of the number
    unsigned int text[6];       //text that contains number in the end
    unsigned char text_fin[7];
    int len = 0;
    int max_indices = 6;

    if(num==0){

        LCD_WriteText(" 0", 2, page, col);

    }else{

        //make negative numbers positive
        if (num < 0){
            numcpy *= -1;
        }

        //write numbers to array with 1000er, 100er, 10er, 1er
        int i = 0;
        for(i = 0; numcpy > 0; i++){
            text[i] = numcpy;
            numcpy = numcpy/10;
            len++;
        }

        //the rest of the text should be 0
        for(i = len; i < max_indices; i++){
            text[i] = 0;
        }

        //those Numbers are converted to ASCII Chars
        int j = 0;
        for( j = 0; j < len; j++){

            //Gets the very right Digit:
            text[j] = (text[j] - (text[j+1] * 10)) + 48;
        }

        //now the final Text is completed
        int k = 0;

        //At first: Write the sign
        if(num < 0){
            text_fin[0] = '-';
        }else{
            text_fin[0] = ' ';
        }
        k = 1;
        len++;

        //Number in text is reversed
        for( ; k < max_indices; k++){
            text_fin[k] = text[len-k-1];
        }

        for(i=len; i <= max_indices; i++){
           text_fin[i] = 0;
        }

        LCD_WriteText(text_fin, 7, page, col);

    }
}

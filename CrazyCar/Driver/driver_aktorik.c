/*
Diese Funktion behandelt Lenkung und Beschleunigung
*/

#include <driver_aktorik.h>
//#include <regler_1.h>
#include <regler_const.h>
#include <controller_pid.h>

extern RPM RPM_Data;
extern PIDContr pid_steering;
extern PIDContr pid_throttle;

int driver_throttle;

unsigned char cntr_pulses;			//Pulscounter

//converts the steering value (-100 to 100) into pwm cycles and writes them to the timer
void Driver_SetSteering(signed int position) {
	
	/*
	Diese Funktion bewegt den Servomotor der Lenkung, da in hal_gpio.c
	das Ergebnis von CCR2 auf Port P3.3 geroutet wurde.
	*/
	
    if ((position >= 0) && (position <= STEER_MAX)){
		
        TA1CCR2 = (position * 10) + 3500;
		//Im Compare Register steht ein Wert zwischen 3500 und 4500(MAX)
		
    } else if((position < 0) && (position >= STEER_MIN)){
		
        TA1CCR2 =  3500 + (position * 10);
		//Im Compare Register steht ein Wert zwischen 2500(MIN) und 3500
    }

}

//initializes the steering in the middle position
void Driver_SetSteeringInit() {
    Driver_SetSteering(0);
}

//converts the throttle value (-100 to 100) into pwm cycles and writes them to the timer
void Driver_SetThrottle(signed int speed) {
    driver_throttle = speed;
    pid_throttle.sollwert = speed;

}

//Regulates the throttle according to the throttle value
void controlThrottle(){

    if (pid_throttle.rev == 0){
        int erg = controllerPIDThrottle();
        if (erg) {
            TA1CCR1 = erg;
        } else {
            return;
        }
    }

}

//call the steering pid and write it if not 0
void controlSteering(){
    int steer = controlPIDSteering();
    if(steer){
        Driver_SetSteering(steer);
    }
}

void Driver_Reverse(int speed){
    TA1CCR1 = (2000 - (speed * 10)) * 2.5;
}

void Driver_ESC_Init() {
	
	/*
	Kalibrieren des Motors
	Pulsbreite in Schritte (Hz*s)
	*/
	
    createPulses(2500,140);
    createPulses(5000,140);
    createPulses(7500,140);
    createPulses(10000,140);
    createPulses(6250,1);
}

void createPulses(int pulsewidth, unsigned char amount) {
	
	//Diese Funktion generiert amount viele Impulse
	
    cntr_pulses = 0;
    while(cntr_pulses <= amount){
        TA1CCR1 = pulsewidth;
    }
}



#pragma vector=TIMER1_A0_VECTOR
   __interrupt void timera0_isr(void)
   {
	   /*
	   Dieser Interrupt wird immer dann ausgeloest wenn der Timer auf
	   den Referenzwert stosst.
	   
	   Durch die reset/set Modus von TimerA bedeutet das einen Impuls.
	   */
	   
       cntr_pulses++;			//Increases the pulscounter by 1
       TA0CCTL0 &= ~CCIFG;	    //Resets the interrupt flag
   }

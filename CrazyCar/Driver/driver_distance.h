#ifndef __DRIVER_DISTANCE__
    #define __DRIVER_DISTANCE__

    #include <hal_adc12.h>
    #include <configuration.h>

    //sensor 1=front, 2=right, 3=left
    //method 1=nofilter, 2=window, 3=weighted_window
    int getDistance(int sensor);
    int getDistanceFast(int sensor);        //nofilter
    int getDistanceSmooth(int sensor);      //window
    int getDistanceSmooth_weighted(int sensor); //weighted window
    int refreshData();

#endif

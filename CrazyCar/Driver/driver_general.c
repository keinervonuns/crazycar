#include <driver_general.h>

//initializes all Drivers
void Driver_Init(void) {
    Driver_SetSteeringInit();
    Driver_ESC_Init();
    Driver_LCD_Init();
}


#ifndef __DRV_AKTORIK__
    #define __DRV_AKTORIK__

    #include <configuration.h>
    #include <msp430.h>
    #include <hal_gpio.h>
    #include <hal_timerA0.h>

    void Driver_SetSteering(signed int position);
    void Driver_SetThrottle(signed int speed);
    void Driver_SetSteeringInit();
    void Driver_ESC_Init();
    void createPulses(int pulsewidth, unsigned char amount);
    void controlThrottle();
    void controlSteering();
    void Driver_Reverse(int);

#endif

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Hardware/debounce.c \
../Hardware/hal_adc12.c \
../Hardware/hal_dma.c \
../Hardware/hal_general.c \
../Hardware/hal_gpio.c \
../Hardware/hal_pmm.c \
../Hardware/hal_timerA0.c \
../Hardware/hal_timerA1.c \
../Hardware/hal_timerB0.c \
../Hardware/hal_ucs.c \
../Hardware/hal_usciB1.c \
../Hardware/hal_wdt.c 

C_DEPS += \
./Hardware/debounce.d \
./Hardware/hal_adc12.d \
./Hardware/hal_dma.d \
./Hardware/hal_general.d \
./Hardware/hal_gpio.d \
./Hardware/hal_pmm.d \
./Hardware/hal_timerA0.d \
./Hardware/hal_timerA1.d \
./Hardware/hal_timerB0.d \
./Hardware/hal_ucs.d \
./Hardware/hal_usciB1.d \
./Hardware/hal_wdt.d 

OBJS += \
./Hardware/debounce.obj \
./Hardware/hal_adc12.obj \
./Hardware/hal_dma.obj \
./Hardware/hal_general.obj \
./Hardware/hal_gpio.obj \
./Hardware/hal_pmm.obj \
./Hardware/hal_timerA0.obj \
./Hardware/hal_timerA1.obj \
./Hardware/hal_timerB0.obj \
./Hardware/hal_ucs.obj \
./Hardware/hal_usciB1.obj \
./Hardware/hal_wdt.obj 

OBJS__QUOTED += \
"Hardware\debounce.obj" \
"Hardware\hal_adc12.obj" \
"Hardware\hal_dma.obj" \
"Hardware\hal_general.obj" \
"Hardware\hal_gpio.obj" \
"Hardware\hal_pmm.obj" \
"Hardware\hal_timerA0.obj" \
"Hardware\hal_timerA1.obj" \
"Hardware\hal_timerB0.obj" \
"Hardware\hal_ucs.obj" \
"Hardware\hal_usciB1.obj" \
"Hardware\hal_wdt.obj" 

C_DEPS__QUOTED += \
"Hardware\debounce.d" \
"Hardware\hal_adc12.d" \
"Hardware\hal_dma.d" \
"Hardware\hal_general.d" \
"Hardware\hal_gpio.d" \
"Hardware\hal_pmm.d" \
"Hardware\hal_timerA0.d" \
"Hardware\hal_timerA1.d" \
"Hardware\hal_timerB0.d" \
"Hardware\hal_ucs.d" \
"Hardware\hal_usciB1.d" \
"Hardware\hal_wdt.d" 

C_SRCS__QUOTED += \
"../Hardware/debounce.c" \
"../Hardware/hal_adc12.c" \
"../Hardware/hal_dma.c" \
"../Hardware/hal_general.c" \
"../Hardware/hal_gpio.c" \
"../Hardware/hal_pmm.c" \
"../Hardware/hal_timerA0.c" \
"../Hardware/hal_timerA1.c" \
"../Hardware/hal_timerB0.c" \
"../Hardware/hal_ucs.c" \
"../Hardware/hal_usciB1.c" \
"../Hardware/hal_wdt.c" 



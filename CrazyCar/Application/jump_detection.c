#include <jump_detection.h>

int front_queue[QUEUESIZE_FFRONT];
int right_queue[QUEUESIZE_RIGHT];
int left_queue[QUEUESIZE_LEFT];
int front_history[QUEUESIZE_FFRONT];
int right_history[QUEUESIZE_RIGHT];
int left_history[QUEUESIZE_LEFT];

int isJump(int sensor, int value){
    switch(sensor) {
    case 0:             //front sensor
        addQueue(front_queue, front_history, value, QUEUESIZE_FFRONT);      //add value to queue
        if (getJump(front_history, QUEUESIZE_FFRONT)){                      //if a jump is detected
            clearHistory(front_history, QUEUESIZE_FFRONT);
            return 1;
        } else {
            return 0;
        }
    case 1:             //right sensor
        addQueue(right_queue, right_history, value, QUEUESIZE_RIGHT);       //add value to queue
        if (getJump(right_history, QUEUESIZE_RIGHT)){                       //if a jump is detected
            clearHistory(right_history, QUEUESIZE_RIGHT);
            return 1;
        } else {
            return 0;
        }
    case 2:             //left sensor
        addQueue(left_queue, left_history, value, QUEUESIZE_LEFT);          //add value to queue
        if (getJump(left_history, QUEUESIZE_LEFT)){                         //if a jump is detected
            clearHistory(left_history, QUEUESIZE_LEFT);
            return 1;
        } else {
            return 0;
        }
    default:
        return 2;
    }
}

//searches for jumps in the history array and decides if it truly is a jump
int getJump(int* history, int size) {
    int sum = 0;
    int idx = 0;

    //sums up the history array
    for (idx = 0; idx < size; idx++){
        sum += history[idx];
    }

    //returns a jump if there are more than TRIGGER_NUMBER jumps in the array
    if (sum >= TRIGGER_NUMBER){
        return 1;
    } else {
        return 0;
    }

}

//adds the next sensor value to the queue
void addQueue(int* queue, int* history, int new_val, int size){
    int i = size - 1;

    //shift the array to the right
    for(i = size - 1; i > 0; i--){
        queue[i] = queue[i-1];
    }

    //add the new value to the beginning
    queue[0] = new_val;

    //if neither right nor left are 0
    if ((right_queue[0] != 0) && (right_queue[size - 1] != 0)){
        //and one of both is more than JUMP_TOLERANCE larger than the other
        if  (queue[0] - queue[size - 1] > JUMP_TOLERANCE){
            //add a 1 (jump) to the history
            addHistory(history, 1, size);
        } else {
            //else add a 0 (no jump)
            addHistory(history, 0, size);
        }
    }
}

//adds the above value to the front of the history array
void addHistory(int* history, int val, int size){
    int i = 0;

    //shifts the array one to the right
    for(i = size - 1; i > 0; i--){
        history[i] = history[i-1];
    }

    //and adds the new value to the beginning
    history[0] = val;
}

//sets the history to all 0
void clearHistory(int* history, int size){
    int i = 0;

    for(i = size - 1; i > 0; i--){
            history[i] = 0;
        }
}


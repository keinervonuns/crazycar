#include <state_methods.h>

extern RPM RPM_Data;
extern distances dist;
extern int steering_value;

//decides wich turn to take
int sideDecider(){
    int method = 0;

    method = jumpSide();

    switch(method){
	//right turn
    case 1:
        return 1;
	//right turn
    case 2:
        return 2;
    //left turn
    case 3:
        return 3;
	//left turn
    case 4:
        return 4;
    default:
        break;
    }
    return 0;
}

//deciding based on jump detection and side distance
int jumpSide(){
	//right turn
    if ((dist.jump_right == 1) && (dist.right > 95)){
		//with braking
        if(RPM_Data.speed_rpm > BRAKERPM_TURN){
            return 2;
		//without braking
        } else return 1;
    }

	//left turn
    if ((dist.jump_left == 1) && (dist.left > 95)){
		//with braking
        if(RPM_Data.speed_rpm > BRAKERPM_TURN){
            return 4;
		//without braking
        } else return 3;
    }

    return 0;
}

int sideFront(){
    if ((dist.side > 40) && (dist.front < 150)){
        if(RPM_Data.speed_rpm > BRAKERPM_TURN){
            return 2;
        } else return 1;
    }

    if ((dist.side < -40) && (dist.front < 150)){
        if(RPM_Data.speed_rpm > BRAKERPM_TURN){
            return 4;
        } else return 3;
    }

    return 0;
}

int jumpOnly(){
    if (dist.jump_right == 1){

        if(RPM_Data.speed_rpm > BRAKERPM_TURN){
            return 2;
        } else return 1;
    }

    if (dist.jump_left == 1){

        if(RPM_Data.speed_rpm > BRAKERPM_TURN){
            return 4;
        } else return 3;
    }

    return 0;
}



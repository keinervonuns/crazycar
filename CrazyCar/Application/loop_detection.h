#ifndef __LOOPDET__

    #define __LOOPDET__

    #include <configuration.h>
    #include <driver_aktorik.h>
    #include <hal_timerA0.h>

    void roundaboutRightHandler();
    void resetRoundaboutDetection();
    void roundaboutStraightExit();
    void roundaboutExitHandler();
    void roundaboutLeftHandler();
    void roundaboutRightHandler();

#endif

#include <display.h>

extern ADC12Com ADC_Data;
extern RPM RPM_Data;
extern PIDContr pid_throttle;
extern distances dist;
extern statevars stb;

int page_nr;

void refreshDisplay() {

    page_nr = 0;

    WriteText("BATT", ADC_Data.battery, autoPage());
    //WriteText("DIFF", dist.side, autoPage());
    WriteText("FRNT", dist.front, autoPage());
    //WriteText("FRNT", ADC_Data.front, autoPage() );
    // WriteText("RGHT", dist.right, autoPage() );
    //WriteText("LEFT", dist.left, autoPage() );
    WriteText("STAT", stb.mystate, autoPage() );
    //WriteText("__Y_", (int) pid_throttle.y, autoPage() );
    //WriteText("___M", getWayM(), autoPage() );
    //WriteText("__CM", getWayCmTotal(), autoPage() );
    //WriteText("BE_F", stb.bowlexit_free, autoPage() );
    //WriteText("SIDE", stb.sidemode, autoPage() );
    //WriteText("EL__", stb.enableleft, autoPage() );
    WriteText("RPM_", RPM_Data.speed_rpm, autoPage() );

    if(stb.direction==RICHTUNG_LINKS){
        //LEFT
        WriteTextOnly("DIR_", "links", autoPage());
    }else if(stb.direction==RICHTUNG_RECHTS){
        //RIGHT
        WriteTextOnly("DIR_", "rechts", autoPage());
    }else{
        WriteTextOnly("DIR_", "error", autoPage());
    }
}

int autoPage() {
    page_nr++;
    return page_nr;
}



typedef struct {
  int left;
  int right;
  int left_old;
  int right_old;
} jump2;

jump2 jumps2;

int isJump2(int sensor, int value){
    switch(sensor){
    case 1:
        jumps2.right_old = jumps2.right;
        jumps2.right = value;

        if ((jumps2.right - jumps2.right_old) > 20){
            return 1;
        } else {
            return 0;
        }
    case 2:
        jumps2.left_old = jumps2.left;
        jumps2.left = value;

        if ((jumps2.left - jumps2.left_old) > 20){
            return 1;
        } else {
            return 0;
        }
    default:
        return 2;
    }
}


#ifndef __STATE_METHODS__
    #define __STATE_METHODS__

    #include <msp430.h>
    #include <configuration.h>
    #include <hal_gpio.h>
    #include <hal_timerA0.h>
    #include <driver_aktorik.h>

	//external function
    int sideDecider();	
	//return:	0-nothnig, 1-right turn without braking, 2-right turn with braking, 
	//			3-left turn wihout braking, 4-left turn with braking

	//internal functions
    int jumpSide();
    int sideFront();
    int jumpOnly();

#endif

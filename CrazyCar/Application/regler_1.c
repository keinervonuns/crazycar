#include <regler_1.h>
#include <configuration.h>
#include <hal_general.h>

extern RPM RPM_Data;

extern int driver_throttle;

int regler1(){


       int input = 0;
       int output = 0;
       int offset = 0;
       int control_throttle = 0;


       //if the throttle is negative (reverse), then dont control it
       if ((driver_throttle < 0) && (driver_throttle >= SPEED_MIN)){
           TA1CCR1 = (2000 + (driver_throttle * 10)) * 2.5;
           return 0;
       //if the throttle value is between 0 and 100
       } else if(driver_throttle <= SPEED_MAX) {
           input = driver_throttle*15;

           //calculate the offset
           offset = input - RPM_Data.speed_rpm;

           //if the offset is positive (car is too slow)
           if(offset > 0){
               //add half of the offset
               output = RPM_Data.speed_rpm + (offset * 0.8);

               //calculate the number of pulses
               control_throttle = output / 15;
               return (3000 + (control_throttle * 10)) * 2.5;

           //else (car is too fast)
           } else if (RPM_Data.speed_rpm > 300){

               output = (offset / 3);      //make it max. 500
               return ( 3000 - (offset / 0.8) );
           }
       }
       return 0;
}

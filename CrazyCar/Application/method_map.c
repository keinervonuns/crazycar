/*
 * method: map
 *
 * this method keeps track of the driven distance and detects curves just with this distance.
 */

#include <method_map.h>
#include <hal_timerA0.h>
//#include <statemachine.h>

int map_nr_loaded;  //nr of the map loaded
int curves[9][3];
/*
 * the variable curves[9][3] contains:
 * curves[curve_nr][0] = cm_total where curve begins
 * curves[curve_nr][1] = cm_total where curve ends
 * curves[curve_nr][2] = value of the direction
 *
 * direction values:
 * 4 ... sharp left curve
 * 2 ... sharp right curve
 */

extern int straight_falg;   //to force straight case

void getStraightFromMap(){

    //Some maps need a straight setting

    int cm_total = getWayCmTotal();     //get total driven distance

    if(map_nr_loaded==2){
        if(cm_total>875 && cm_total<920){
            straight_falg=1;
        }
    }

}

int methodMap(){

    int cm_total = getWayCmTotal();     //get total driven distance

    if((cm_total>curves[0][0]) && (cm_total<curves[0][1])){
        return curves[0][2];
    }

    if((cm_total>curves[1][0]) && (cm_total<curves[1][1])){
        return curves[1][2];
    }

    if((cm_total>curves[2][0]) && (cm_total<curves[2][1])){
        return curves[2][2];
    }

    if((cm_total>curves[3][0]) && (cm_total<curves[3][1])){
        return curves[3][2];
    }

    if((cm_total>curves[4][0]) && (cm_total<curves[4][1])){
        return curves[4][2];
    }

    if((cm_total>curves[5][0]) && (cm_total<curves[5][1])){
        return curves[5][2];
    }

    if((cm_total>curves[6][0]) && (cm_total<curves[6][1])){
        return curves[6][2];
    }

    if((cm_total>curves[7][0]) && (cm_total<curves[7][1])){
        return curves[7][2];
    }

    if((cm_total>curves[8][0]) && (cm_total<curves[8][1])){
        return curves[8][2];
    }

    return 0;
}

void loadMap1(){

    map_nr_loaded=1;
    /*
     * Direction:   left
     * Source:      measured distance
     * Distance:    ???
     */

    int curve_normal_remain=40;
    int curve_uturn_remain=75;

    //1. normale linkskurve
    curves[0][0]    = 500;
    curves[0][1]    = curves[0][0]+curve_normal_remain;
    curves[0][2]    = 4;

    //2. normale linkskurve
    curves[1][0]    = 640;
    curves[1][1]    = curves[1][0]+curve_normal_remain;
    curves[1][2]    = 4;

    //2.1 kurz rechts
    curves[2][0]    = 836;
    curves[2][1]    = curves[2][0]+15;
    curves[2][2]    = 1;

    //3. U-Turn links
    curves[3][0]    = 851;
    curves[3][1]    = curves[3][0]+curve_uturn_remain;
    curves[3][2]    = 4;

    //4. U-Turn rechts
    curves[4][0]    = 1111;
    curves[4][1]    = curves[4][0]+curve_uturn_remain;
    curves[4][2]    = 2;

    //5. Kurve rechts
    curves[5][0]    = 1449;
    curves[5][1]    = curves[5][0]+curve_normal_remain;
    curves[5][2]    = 2;

    //6. Kurve links
    curves[6][0]    = 1600;
    curves[6][1]    = curves[6][0]+curve_normal_remain;
    curves[6][2]    = 4;

    //7. Kurve links
    curves[7][0]    = 1730;
    curves[7][1]    = curves[7][0]+curve_normal_remain;
    curves[7][2]    = 4;

    //8. Kurve rechts
    curves[8][0]    = 1921;
    curves[8][1]    = curves[8][0]+curve_normal_remain;
    curves[8][2]    = 4;

}


void loadMap2(){

    map_nr_loaded=2;
    /*
     * Direction:   right
     * Source:      measured distance
     * Distance:    aprx. 2040cm
     */

    int curve_normal_remain=40;
    int curve_uturn_remain=75;

    //1. normale rechts
    curves[0][0]    = 450;
    curves[0][1]    = curves[0][0]+curve_normal_remain;
    curves[0][2]    = 2;

    //2. normale rechts
    curves[1][0]    = 650;
    curves[1][1]    = curves[1][0]+curve_normal_remain;
    curves[1][2]    = 2;


    //3. normale rechts
    curves[2][0]    = 800;
    curves[2][1]    = curves[2][0]+20;
    curves[2][2]    = 2;

    //4. normale links
    curves[3][0]    = 920;
    curves[3][1]    = curves[3][0]+(curve_normal_remain+60);
    curves[3][2]    = 4;

    //5.1 gegenlenken
    curves[4][0]    = (1252-15);
    curves[4][1]    = curves[4][0]+15;
    curves[5][2]    = 1;

    //5.2 u-turn links
    curves[5][0]    = 1252;
    curves[5][1]    = curves[5][0]+curve_uturn_remain;
    curves[5][2]    = 4;

    //6 u-turn rechts
    curves[6][0]    = 1480;
    curves[6][1]    = curves[6][0]+curve_uturn_remain;
    curves[6][2]    = 2;

    //6 u-turn rechts
    curves[7][0]    = 1766;
    curves[7][1]    = curves[7][0]+curve_normal_remain;
    curves[7][2]    = 2;

    //8. Kurve rechts
    curves[8][0]    = 1991;
    curves[8][1]    = curves[8][0]+curve_normal_remain;
    curves[8][2]    = 2;

}

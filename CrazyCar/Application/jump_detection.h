
#ifndef __JUMP_DETECTION__
    #define __JUMP_DETECTION__

    #define QUEUESIZE_FFRONT 10
    #define QUEUESIZE_RIGHT 5
    #define QUEUESIZE_LEFT 5
    #define JUMP_TOLERANCE 25
    #define TRIGGER_NUMBER 2

    int isJump(int sensor, int value);
    int getJump(int* history, int size);
    void addQueue(int* queue, int* history, int new_val, int size);
    void addHistory(int* history, int val, int size);
    void clearHistory(int* history, int size);

#endif

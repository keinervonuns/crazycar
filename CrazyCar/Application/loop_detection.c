//======================================
// LOOP detecton
//======================================
#include <loop_detection.h>

extern statevars stb;
extern distances dist;

loopvars loop;

void resetRoundaboutDetection(){
    loop.take_next_out=0;
    loop.roundabout=0;
    loop.i_exit=0;
    loop.exit_cm[0]=0;
    loop.exit_cm[1]=0;
    loop.exit_cm[2]=0;
}

void roundaboutStraightExit(){
    if(loop.roundabout && (dist.front>130) ){
        stb.keep_cm = getWayCmTotal();
        state = KEEPSTRAIGHT;
        stb.straightlength = 50;
        //blink_lcd();
        loop.take_next_out=0;
        loop.roundabout=0;
        loop.i_exit=0;
        loop.exit_cm[0]=0;
        loop.exit_cm[1]=0;
        loop.exit_cm[2]=0;
    }
}

void roundaboutExitHandler(){
    if(loop.roundabout && loop.i_exit==2){
       if((loop.exit_cm[1] - loop.exit_cm[0]) < 80){
           loop.take_next_out=1;
       }else{
           loop.exit_cm[0] = loop.exit_cm[1];
           //take_next_out=0;
           loop.i_exit=1;
       }
   }

}

void roundaboutLeftHandler(){
    //if(loop.roundabout && (dist.right > 50)){
    if(loop.roundabout && dist.jump_right){
        if(loop.take_next_out){
            resetRoundaboutDetection();
            stb.steerstart = getWayCmTotal();
            state = LOOP_LEFT;
            stb.failstart = getWayCmTotal();
            loop.roundabout=0;
        }else{
            loop.exit_cm[loop.i_exit] = getWayCmTotal();
            loop.i_exit++;
        }
    }
}

void roundaboutRightHandler(){
    //if(loop.roundabout && (dist.left > 50)){
    if(loop.roundabout && dist.jump_left){
       if(loop.take_next_out){
           resetRoundaboutDetection();
           stb.steerstart = getWayCmTotal();
           state = LOOP_RIGHT;
           stb.failstart = getWayCmTotal();
           loop.roundabout=0;
       }else{
           loop.exit_cm[loop.i_exit] = getWayCmTotal();
           loop.i_exit++;
       }
    }
}

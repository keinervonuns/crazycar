
#ifndef __CONTROLLER_PID__
    #define __CONTROLLER_PID__

    #include <hal_timerA0.h>
    #include <configuration.h>

    typedef struct
    {
        float e;
        float kp;
        float ki;
        float kd;
        float fkp;
        float fki;
        float fkd;
        float rkp;
        float rki;
        float rkd;
        float ta;
        float satUp;
        float satLow;

        float esum;
        float eold;
    } PID;

    typedef struct
    {
        int enabled;
        int rev;
        float sollwert;
        float istwert;
        float y;

        PID parameter;
    }PIDContr;

    void controllerInit();
    int controllerPIDThrottle();
    int controlPIDSteering();
    void resetPID();

#endif

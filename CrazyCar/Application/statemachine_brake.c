#include <statemachine_brake.h>

extern ButtonCom button;
extern ADC12Com ADC_Data;
extern RPM RPM_Data;
extern PIDContr pid_throttle;
extern PIDContr pid_steering;
extern loopvars loop;

extern int blink;
extern int way_cm_total;

statevars stb;
distances dist;

int straight_falg;
int debug;
int debug1;
int flag;
int straightright;
int failcnt;

void statemachine_brake() {
    stb.compare = getWayCmTotal();


    pid_steering.enabled = 0;
    pid_throttle.enabled = 1;
    pid_throttle.rev = 0;

    dist.front = getDistance(SENS_FRONT);
    dist.right = getDistance(SENS_RIGHT);
    dist.left = getDistance(SENS_LEFT);
    dist.side = dist.right - dist.left;

    dist.jump_front = isJump(SENS_FRONT, dist.front);
    dist.jump_right = isJump(SENS_RIGHT, dist.right);
    dist.jump_left = isJump(SENS_LEFT, dist.left);

    if((stb.mystate != 1) && !(P1IN & STOP_BUTTON)){
        //If Pin sees high
        state = INIT;
        resetfailtimer();   //Back to debounce time
        //button.active = 0;
    }

    if(button.active == 1) {
        if( button.button == 7 ){
            if(stb.mystate == 1){

               if(stb.direction == RICHTUNG_RECHTS){
                   stb.direction = RICHTUNG_LINKS;
               }else if(stb.direction == RICHTUNG_LINKS){
                   stb.direction = RICHTUNG_RECHTS;
               }else{
                   stb.direction = RICHTUNG_LINKS;
               }
               //stb.direction = 3;
               button.active = 0;
           }else{

           }
       }
    }

    switch(state) {
        case INIT:
            stb.speeddivider = 3.7;
            LCD_BACKLIGHT_ON;
            //roundabout detection
            resetRoundaboutDetection();

            pid_throttle.enabled = 0;
            pid_steering.enabled = 0;
            Driver_SetSteeringInit();
            Driver_SetThrottle(0);
            Driver_Reverse(0);

            if((button.active == 1) && (button.button == 6)) {
                LCD_BACKLIGHT_OFF;
                state = STRAIGHT;
                button.active = 0;
                /*
                if(stb.failtime==0){
                    startFailTimer();
                }
                */
            }

            resetPID();

            initStatevars();

            stb.mystate = 1;
            break;

        case STRAIGHT:
            loop.roundabout = 0;
            if(stb.failtime==0){
                startFailTimer();
            }

            if (stb.bowlexit_bridge){
                stb.speeddivider = 4.5;
            } else if (ADC_Data.battery < 2250) {
                stb.speeddivider = 3.7;
            } else {
                stb.speeddivider = 3.7;
            }

            debug1 = 0;

            //LCD_BACKLIGHT_OFF;

            pid_steering.enabled = 1;

            if((dist.front < 150) && (RPM_Data.speed_rpm > BRAKERPM_STRAIGHT)){
               stb.throttle = -40;
               debug = 0;
            } else if (stb.throttle < BRAKETO){
            //if (stb.throttle < BRAKETO){
               stb.throttle = BRAKETO;
               debug = 0;
            } else {
               stb.throttle = (dist.front / stb.speeddivider);
               debug = 1;
               if (stb.throttle < 0){
                   stb.throttle = 0;
               }
            }


            Driver_SetThrottle(stb.throttle);

            int test = sideDecider();
            switch(test){
                case 1:
                case 2:
                    getRight();

                    if (stb.compare > (stb.straightstart + 350)) {
                        mem(2);
                        if((stb.direction == RICHTUNG_LINKS) && (dist.front > 50)){
                            state = RPOINT3_1;
                            resetfailtimer();
                        }else{
                            initStatevars();
                        }
                    }

                    if (dist.front > 120){
                        mem(3);
                        stb.enablelong180 = 1;
                    } else {
                        stb.enablelong180 = 0;
                    }

                    stb.steerstart = getWayCmTotal();

                    break;
                case 3:
                case 4:
                    getLeft();

                    if (stb.compare > (stb.straightstart + 350)) {
                        if ((stb.direction == RICHTUNG_RECHTS) && (dist.front > 50)) {
                            state = LPOINT3_1;
                            resetfailtimer();
                        } else {
                            initStatevars();
                        }
                    }

                    if (stb.bowlexit_free) {
                        stb.speeddivider = 4;
                    }

                    if (dist.front > 120){
                        stb.enablelong180 = 1;
                        mem(3);
                    } else {
                        stb.enablelong180 = 0;
                    }

                    stb.steerstart = getWayCmTotal();

                    break;

                default:
                    /*
                    //========================================================================
                    //AUSSERHALB VOM DEFAULT??

                    if (stb.failtime == 2){
                        stb.failtime=0;
                        failcheckBack();
                    } else if (stb.failtime == 0) {
                        failcheckBack();
                    }
                    //========================================================================
                    */

                    if ((dist.front < stb.emdist)) {
                        stb.emturn = 1;
                        if (stb.bowlexit_bridge){
                            getLeft();
                        } else if (dist.side >= 0) {
                            getRight();
                        } else {
                            getLeft();
                        }
                    } else {
                        pid_steering.enabled = 1;
                        controlSteering();

                    }
                    break;
            }

            if (stb.compare > (stb.straightstart + 250)){
                stb.bowlexit_free = 0;
                stb.speeddivider = 4;
            }

            //========================================================================
            //AUSSERHALB VOM DEFAULT??
            if (stb.failtime == 2){

                failcheckBack();
                //startFailTimer();

            }

            //========================================================================

            stb.mystate = 2;
            break;

        case RIGHT:

            if((stb.compare - stb.steerstart) > 200 ){
                loop.roundabout=1;
            }

            roundaboutRightHandler();

            if (stb.failtime == 2){
               stb.failtime = 0;
            }

            //if (RPM_Data.speed_rpm > BRAKERPM_TURN){
            //    Driver_SetThrottle(-50);
            //} else if (stb.emturn) {
            if (stb.emturn) {
                Driver_SetThrottle(TURNSPEED-15);
            } else {
                Driver_SetThrottle(TURNSPEED);
            }

            if (stb.compare > stb.steerwait){
                Driver_SetSteering(100);
            }

            //if((dist.left>60) && (dist.right>60) && (dist.jump_left)) {

                // If A Right turn opens up
                //stb.bowlexit_free=1;
            //}

            if ((stb.bowlexit_bridge == 1) && (dist.front < 50)){
                state = KEEPRIGHT;
                resetfailtimer();
                //stb.bowlexit_bridge = 0;
            } else if ((stb.bowlexit_bridge == 1) && (dist.jump_left) && ((stb.compare - stb.steerstart) > 80)) {
                state = KEEPRIGHT;
                resetfailtimer();
                //stb.bowlexit_bridge = 0;
            } else if ((stb.bowlexit_bridge == 1) && (dist.right < 40)) {
                state = KEEPRIGHT;
                resetfailtimer();
                //stb.bowlexit_bridge = 0;
            } else if ((dist.left > 60) && (dist.right > 60) && (stb.bowlexit_free == 1) && (dist.jump_left) && (stb.compare > (stb.steerstart + 80))){
                //getStraightRight();
                getLeft();
            } else if ((dist.front > 115) && (dist.right < 65) && (dist.left < 65)) {
                getStraightRight();
            } else if (stb.enablelong180 && (dist.front > 199) && (dist.right < 50)){
                getStraightRight();
            } else if (dist.front > 240) {
                getStraightRight();
            }

            if ((dist.front > 150)  && ((stb.compare - stb.steerstart) > 80)){
                getStraightRight();
                flag = 1;
            }

            if((dist.front < 20) || (stb.failtime != 1)){
                failcheckBack();
            }

            stb.mystate = 3;
            break;

        case LEFT:
            //LCD_BACKLIGHT_ON;

            //roundabout detection
            if((stb.compare - stb.steerstart) > 200 ){
                loop.roundabout=1;
            }

            roundaboutLeftHandler();

            if((dist.front < 20) || (RPM_Data.speed_rpm == 0)){
                state = FAILSAVE_BACK;
                resetfailtimer();
                stb.failstart = getWayCmTotal();
                break;
            }

            //if (RPM_Data.speed_rpm > BRAKERPM_TURN){
                //Driver_SetThrottle(-50);
            //} else if (stb.bowlexit_free) {
            if (stb.bowlexit_free){
                Driver_SetThrottle(TURNSPEED-15);
            } else if (stb.bowlexit_bridge) {
                Driver_SetThrottle(TURNSPEED-10);
            } else if (stb.emturn){
                Driver_SetThrottle(TURNSPEED-15);
            } else {
                Driver_SetThrottle(TURNSPEED);
            }

            if (stb.bowlexit_bridge && (dist.front > 60)){
                //if(dist.right < 25){
                //    Driver_SetSteering((dist.right*4)-100);
                //}
                Driver_SetSteering(0);
            } else {
                Driver_SetSteering(-100);
            }


            if ((stb.bowlexit_free == 1) && (dist.front > 200)){
                getStraightLeft();
            } else if ((dist.front > 130) && (dist.right < 65) && (dist.left < 65)) {
                getStraightLeft();
            } else if (stb.enablelong180) {
                if((dist.front > 199) && (dist.left < 50)){
                    getStraightLeft();
                }
            } else if (dist.front > 240) {
                getStraightLeft();
            }

            if((dist.front > 150) && ((stb.compare - stb.steerstart) > 80)){
                getStraightLeft();  //!!
                flag = 1;
            }

            stb.mystate = 4;
            break;

        case FAILSAVE_BACK:
           loop.roundabout = 0;

           if(stb.failtime==0){
               startFailTimer();
           }

            pid_throttle.enabled = 0;
            pid_throttle.rev = 1;

            Driver_SetSteering(0);
            Driver_Reverse(50);

            if((stb.failstart + 30) < stb.compare)  {
                pid_throttle.rev = 0;
                stb.straightlength=10;
                state = STRAIGHT;
                resetfailtimer();
                Driver_SetSteering(0);
                stb.failstart = 0;

            }
            //=====================================================================================================0
            //if((stb.failstart + 5) < stb.compare){
            if(stb.failtime==2){
                failcnt++;
                //failcheckFront();
                if ((dist.front > 40) || (RPM_Data.speed_rpm == 0)){
                    stb.emturn = 1;
                    if (stb.bowlexit_bridge){
                        getLeft();
                    } else if (dist.side >= 0) {
                        getRight();
                    } else {
                        getLeft();
                    }
                }
            }

            if (failcnt > 5) {
                state = KEEPSTRAIGHT;
                resetfailtimer();
                Driver_SetSteering(0);
                stb.keep_cm = getWayCmTotal();
                stb.straightlength = 20;
                stb.failtime=0;

            }

            stb.mystate = 6;
            break;

        case LOOP_RIGHT:
            pid_throttle.enabled = 0;
            pid_throttle.rev = 1;

            failcheckFront();

            Driver_SetSteering(0);
            Driver_Reverse(50);

            if((stb.failstart + 30) < stb.compare) {
                pid_throttle.rev = 0;
                getLeft();
            }

            stb.mystate = 7;
            break;


        case LOOP_LEFT:
            pid_throttle.enabled = 0;
            pid_throttle.rev = 1;

            failcheckFront();

            Driver_SetSteering(0);
            Driver_Reverse(50);

            if((stb.failstart + 30) < stb.compare)  {
                pid_throttle.rev = 0;
                getRight();
            }

            stb.mystate = 8;
            break;

        case RPOINT3_1:
            //pid_throttle.enabled = 0;
            Driver_SetSteering(0);
            Driver_SetThrottle(20);

            failcheckFront();

            if(dist.front < 25){
                state = RPOINT3_2;
                resetfailtimer();
            }

            stb.mystate = 9;
            break;

        case RPOINT3_2:
            pid_throttle.enabled = 0;
            pid_throttle.rev = 1;

            Driver_SetSteering(100);
            Driver_Reverse(55);

            failcheckFront();

            if(dist.front > 50) {
                pid_throttle.rev = 0;
                getLeft();
            }

            stb.mystate = 10;
            break;

        case LPOINT3_1:
            //pid_throttle.enabled = 0;
            Driver_SetSteering(0);
            Driver_SetThrottle(20);

            failcheckFront();

            if(dist.front < 25){
                state = LPOINT3_2;
                resetfailtimer();
            }

            stb.mystate = 11;
            break;

        case LPOINT3_2:
            pid_throttle.enabled = 0;
            pid_throttle.rev = 1;

            failcheckFront();

            Driver_SetSteering(-100);
            Driver_Reverse(45);

            if(dist.front > 60) {
                pid_throttle.rev = 0;
                getRight();
            }

            stb.mystate = 12;
            break;

        case KEEPSTRAIGHT:

            if(stb.failtime==0){
                startFailTimer();
            }

            Driver_SetSteering(0);

            if(dist.front > 50){
                if((stb.compare - stb.keep_cm) > stb.straightlength ){
                    state=STRAIGHT;
                    resetfailtimer();
                    stb.keep_cm=0;
                }else{
                    Driver_SetThrottle(60);
                }
            } else {
                if (stb.bowlexit_bridge){
                    getLeft();
                }if (dist.side >= 0) {
                    getRight();
                } else {
                    getLeft();
                }
            }

            if(stb.failtime==2){
                failcheckBack();

            }


            stb.mystate = 13;

            break;

        case KEEPRIGHT:

            if(stb.failtime==0){
                startFailTimer();
            }

            stb.steer = (dist.right - 20) * 3;

            if (stb.steer < -30){
                stb.steer = -30;
            }

            if (stb.steer > 30){
                stb.steer = 30;
            }

            Driver_SetSteering(stb.steer);

            if (dist.jump_left){
                getLeft();
            } else {
                if ((dist.front < stb.emdist)) {
                    stb.emturn = 1;
                    if (stb.bowlexit_bridge){
                        getLeft();
                    } else if (dist.side >= 0) {
                        getRight();
                    } else {
                        getLeft();
                    }
                }
            }

            if(stb.failtime==2){
                failcheckBack();
            }

            stb.mystate = 14;

            break;
    }

    /*
    if (stb.mystate == 6){
        LCD_BACKLIGHT_ON;
    } else {
        LCD_BACKLIGHT_OFF;
    }*/


    roundaboutExitHandler();
    roundaboutStraightExit();
    controlThrottle();
    //LCD_BACKLIGHT_TOGGLE;
}

void holdRight(){
    pid_steering.sollwert = -SIDEOFFSET;
    stb.sidemode = 0;
}

void holdLeft(){
    pid_steering.sollwert = SIDEOFFSET;
    stb.sidemode = 1;
}

void getStraightLeft(){
    state = STRAIGHT;
    resetfailtimer();

    Driver_SetSteering(0);

    if (stb.bowlexit_bridge) {
        initStatevars();
    }

    mem(0);

    if (flag) {
        state = KEEPSTRAIGHT;
        resetfailtimer();
        Driver_SetSteering(0);
        stb.keep_cm = getWayCmTotal();
        stb.straightlength = 50;
        stb.failtime=0;
        flag = 0;
    }

    stb.emturn = 0;
    stb.enablelong180 = 0;
    stb.bowlexit_free = 0;
    stb.speeddivider = 3.7;
    stb.bowlexit_bridge = 0;
    stb.straightstart = getWayCmTotal();

    if (((stb.compare - stb.steerstart) > 100) && (stb.bowlexit_free == 0)) {
        holdLeft();
    } else {
        holdRight();
    }

}

void getStraightRight(){
    state = STRAIGHT;
    resetfailtimer();

    Driver_SetSteering(0);

    if (stb.enablelong180){
        //initStatevars();
    }

    if (stb.enablelong180 == 0){
        mem(1);
    }

    stb.emturn = 0;
    stb.enablelong180 = 0;
    stb.straightstart = getWayCmTotal();

    if (((stb.compare - stb.steerstart) > 100) && (stb.enablelong180 == 0))  {
        stb.bowlexit_bridge = 1;
    } else {
        //stb.bowlexit_bridge = 0;
    }

    if (flag) {
        state = KEEPSTRAIGHT;
        resetfailtimer();
        Driver_SetSteering(0);
        stb.keep_cm = getWayCmTotal();
        stb.straightlength = 50;
        stb.failtime=0;
        flag = 0;
    }

    stb.speeddivider = 3.7;

    holdLeft();

}

void initStatevars(){
    //LCD_BACKLIGHT_ON;
    stb.steerwait = 0;
    stb.enablelong180 = 0;
    stb.steer = 0;
    stb.compare = 0;
    stb.sidemode = 0;
    stb.steerstart = 0;
    stb.bowlexit_bridge = 0;
    stb.bowlexit_free = 0;
    stb.straightstart = 0;
    stb.failstart = 0;
    stb.emturn = 0;
    stb.emdist = 60;
}

void blink_lcd(){
    if (stb.mystate != 1){
        TA2CCR0 = 3000;
        //LCD_BACKLIGHT_ON;
        stb.blinklcd=1;
        TA2R = 0x0000;           // reset timer A2 register
        TA2CTL |= MC_1;          // set timer A2 to count UP
    }
}

void startFailTimer(){
    if (stb.mystate != 1){
        TA2CCR0 = 10000;
        stb.failtime=1;
        TA2R = 0x0000;           // reset timer A2 register
        TA2CTL |= MC_1;          // set timer A2 to count UP
    }
}

void failcheckBack(){
    if(RPM_Data.speed_rpm == 0){
        state = FAILSAVE_BACK;
        resetfailtimer();
        stb.failstart = getWayCmTotal();
        stb.failtime=0;
    }
}

void failcheckFront(){
    if(RPM_Data.speed_rpm == 0){
        state = STRAIGHT;
        resetfailtimer();
        Driver_SetSteering(0);
        stb.keep_cm = getWayCmTotal();
        stb.failtime=0;
    }
}

void mem(int val){
    stb.mem[2] = stb.mem[1];
    stb.mem[1] = stb.mem[0];
    stb.mem[0] = val;

    if (((stb.mem[0] == 1) && (stb.mem[1] == 1) && (stb.mem[2] == 2)) || (stb.mem[1] == 3)){
        stb.emdist = 40;
    } else {
        stb.emdist = 60;
    }

    if ((stb.mem[0] == 1) && (stb.mem[1] == 1)){
        stb.bowlexit_free = 1;
    }

}

void resetfailtimer(){
    TA2CTL &= ~MC_1;         // stops timer A2 by unsetting count-up
    TA2R = 0x0000;           // reset timer register
    TA2CCR0 = 1000;
    stb.failtime=0;
}

void getRight(){
    state = RIGHT;
    startFailTimer();
    stb.steerwait = getWayCmTotal();
    Driver_SetSteering(100);

}

void getLeft(){
    state = LEFT;
    startFailTimer();
    stb.steerwait = getWayCmTotal();
    Driver_SetSteering(-100);
}



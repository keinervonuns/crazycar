#ifndef __APPL_DISPLAY__
    #define __APPL_DISPLAY__

    #include <configuration.h>
    #include <driver_distance.h>
    #include <hal_adc12.h>
    #include <hal_general.h>
    #include <driver_lcd.h>
    #include <hal_timerA0.h>
    #include <controller_pid.h>

    void refreshDisplay();
    int autoPage();

#endif

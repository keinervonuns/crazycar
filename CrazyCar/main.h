
#ifndef __MAIN__
    #define __MAIN__

    #include <msp430.h>
    #include <configuration.h>
    #include <hal_general.h>
    #include <hal_gpio.h>
    #include <driver_general.h>
    #include <hal_usciB1.h>
    #include <driver_lcd.h>
    #include <hal_adc12.h>
    #include <hal_timerA0.h>
    #include <driver_distance.h>
    #include <controller_pid.h>

    #include <display.h>
    #include <hal_wdt.h>

    //#include <statemachine_template.h>

    //#include <statemachine_test.h>
    #include <statemachine_brake.h>
#endif



#ifndef __CONFIGURATION__
    #define __CONFIGURATION__

    //algorithm Settings
    #define SIDE_TOLERANCE  2               //tolerance to side
    #define COUNTERSTEER 5                  //Step size in which the steering is compensated
    #define STRAIGHTSPEED_OFFSET 0          //straight speed id max. 75 + offset
    #define TURNSPEED 35                    //speed in turns
    #define BRAKETO 25
    #define TURNSPEED_BRAKE 40
    #define BRAKERPM_STRAIGHT 900          //RPM above which it brakes when it detects a wall
    #define BRAKERPM_TURN 900               //RPM above at which it brakes when it detects a turn
    #define BRAKESTRENGTH_STRAIGHT 50       //Brakestrength (0/none , 100/hard)
    #define BRAKESTRENGTH_TURN -50          //Brakestrength == speed
    #define BRAKESTEER_ANGLE 98             //Steering angle for the BRAKER/L conditions
    #define BRAKEDURATION_STRAIGHT 6        //number of cycles to brake in straight state
    #define BRAKEDURATION_SIDE 4            //number of cycles to brake in side state
    #define STEERSTART_ANGLE 98             //Steering for the beginning of the turn
    #define SIDEOFFSET 20

    //Steering settings
    #define STEER_MAX   100     //maximum steering angle right
    #define STEER_MIN   -100    //maximum steering angle left
    #define SPEED_MAX   100     //maximum speed forward
    #define SPEED_MIN   -100    //maximum speed backward
    #define BRAKE_MIN   100
    #define BRAKE_MAX   200

    //Driving Direction
    #define RICHTUNG_RECHTS 0
    #define RICHTUNG_LINKS 1

    //enumeration for the States
    enum {
      INIT,
      STRAIGHT,
      LEFT,
      RIGHT,
      FAILSAVE_BACK,
      LOOP_RIGHT,
      LOOP_LEFT,
      KEEPSTRAIGHT,
      RPOINT3_1,
      RPOINT3_2,
      LPOINT3_1,
      LPOINT3_2,
      KEEPRIGHT,
    }state;

    //enumerations for the sensors
    enum {
        SENS_FRONT,
        SENS_RIGHT,
        SENS_LEFT,
    };

    typedef struct {
        int steerwait;
        char backend;
        char enableleft;
        char enablelong180;
        signed char steer;
        int compare;
        char sidemode;
        int steerstart;
        char bowlexit_bridge;
        char bowlexit_free;
        int straightstart;
        char mystate;
        signed char throttle;
        char direction;
        int failstart;
        char blinklcd;
        char emturn;
        char emdist;
        char mem[3];
        int straightlength;
        float speeddivider;
        int keep_cm;
        int failtime;
    } statevars;

    typedef struct {
        int front;
        int left;
        int right;
        int side;
        char jump_right;
        char jump_left;
        char jump_front;
    } distances;

    typedef struct {
        int roundabout;
        int exit_cm[3];
        int i_exit;
        int take_next_out;
    } loopvars;

#endif
